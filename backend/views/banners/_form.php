<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\imagine\Image;

/**
 * @var yii\web\View $this
 * @var app\models\Banners $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="banners-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>


    <?= $form->field($model, 'title')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'reference')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, '_image')->fileInput() ?>
    <?php if($image = $model->getImage()): ?>
        <?php
        $image = $model->getImage()->getUrl('600x120'); ?>
        <img src="<?=$image?>">
        <?= $form->field($model, '_deleteimage')->checkBox() ?>      
    <?php endif; ?>

    <?= $form->field($model, 'place')->dropDownList(
            array(
                'bottom'=>'Низ страницы'
                ),         
            ['prompt'=>'-']
        );
     ?>

    <?= $form->field($model, 'is_visible')->checkBox() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
