<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\imperavi\Widget;
/**
 * @var yii\web\View $this
 * @var app\models\Pages $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'id_parent')->dropDownList(
            $parent,         
            ['prompt'=>'-']
        );
     ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'path')->textInput(['maxlength' => 128]) ?>

    <?= yii\imperavi\Widget::widget([
            'model' => $model,
            'attribute' => 'text',
            'options' => [
                'lang' => 'ru',
                'imageUpload' => '/site/upload-image',
                'fileUpload' => '/site/upload-file',
            ],
    ]); ?>

    <?= $form->field($model, 'is_visible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
