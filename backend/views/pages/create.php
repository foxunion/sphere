<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Pages $model
 */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Текстовые страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'parent' => $parent,
    ]) ?>

</div>
