<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsersGroupsRules $model
 */

$this->title = 'Добавить правило';
$this->params['breadcrumbs'][] = ['label' => 'Права на пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-groups-rules-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
