<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\UsersGroupsRules $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="users-groups-rules-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_group')->textInput() ?>

    <?= $form->field($model, 'table')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'option')->textInput(['maxlength' => 45]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
