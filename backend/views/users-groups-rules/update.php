<?php

use yii\helpers\Html;
use yii\bootstrap\Alert;
/**
 * @var yii\web\View $this
 * @var app\models\UsersGroupsRules $model
 */

$this->title = 'Редактировать правило: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Права на пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;
?>
<div class="users-groups-rules-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
