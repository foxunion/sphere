<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsersGroups $model
 */

$this->title = 'Добавить пакет';
$this->params['breadcrumbs'][] = ['label' => 'Пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-groups-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'usersGroupsOptionsModel' => $usersGroupsOptionsModel,
    ]) ?>

</div>
