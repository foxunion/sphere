<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\UsersMessageSearch $searchModel
 */

$this->title = 'Личные сообщения';
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;

?>
<div class="users-message-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Отправить личное сообщение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Отправитель',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->idSender->uid;
                },
            ],
            [
                'attribute' => 'Получатель',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->idRecipient->uid;
                },
            ],
            [
                'attribute' => 'Тема',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->subject;
                },
            ],
            [
                'attribute' => 'Сообщение',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->text;
                },
            ],
            [
                'attribute' => 'Тип сообщения',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->type ? 'Личное сообщение' : 'Системное сообщение';
                },
            ],
            [
                'attribute' => 'Статус',
                'format' => 'raw',
                'value' => function ($model) {  
                        switch ($model->status) {
                            case 0:
                                $status = 'Не прочитано';
                                break;
                            case 1:
                                $status = '<b>Прочитано</b>';
                                break;
                            case 2:
                                $status = 'Удалено';
                                break;
                        }                    
                        return $status;
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>

</div>
