<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsersMessage $model
 */

$this->title = 'Отправить личное сообщение';
$this->params['breadcrumbs'][] = ['label' => 'Личные сообщения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'users' => $users,
    ]) ?>

</div>
