<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsersPhones $model
 */

$this->title = 'Добавить телефон';
$this->params['breadcrumbs'][] = ['label' => 'Телефоны пользователя', 'url' => ['index', 'id' => $_GET['id']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-phones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
