<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\imagine\Image;

/**
 * @var yii\web\View $this
 * @var app\models\ServicesCategory $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="services-category-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'parent_id')->dropDownList(
            $parent,         
            ['prompt'=>'-']
        );
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 128]) ?>
    
    <?= $form->field($model, 'keyword')->textInput(['maxlength' => 128]) ?>
    
    <?= $form->field($model, '_image')->fileInput() ?>
    <?php if($image = $model->getImage()): ?>
        <?php
        $image = $model->getImage()->getUrl('120x120'); ?>
        <img src="<?=$image?>">
        <?= $form->field($model, '_deleteimage')->checkBox() ?>      
    <?php endif; ?>

    <?= $form->field($model, 'position')->textInput() ?>
    

    <?= $form->field($model, 'is_visible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
