<?php
use yii\helpers\Html;
?>

<?= Html::a($data->title, ['services/index', 'id' => $data->id]) ?> 
<?= Html::a('<span class="glyphicon glyphicon-pencil">', ['update', 'id' => $data->id]) ?> 
<?= Html::a('<span class="glyphicon glyphicon-trash">', ['delete', 'id' => $data->id]) ?>

<div class="child" style="padding-left: 20px;">
<?php 
foreach ($data->servicesCategories as $key => $data)
	echo $this->render('_view',
	 	array('data'=>$data)
	);
?>
</div>