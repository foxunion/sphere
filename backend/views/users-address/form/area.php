<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\UsersAddressArea $model
 * @var ActiveForm $form
 */
?>
<div class="usersaddress-form-area">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'id_city')->dropDownList(
                $data,         
                ['prompt'=>'-']
            );
        ?>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'is_visible')->checkbox(); ?>
    
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- usersaddress-form-area -->
