<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\UsersAddress $model
 * @var ActiveForm $form
 */
?>
<div class="users-address-form-address">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'description') ?>
        <?= $form->field($model, 'id_region')->dropDownList(
                $region,         
                ['prompt'=>'-']
            );
        ?>
        <?= $form->field($model, 'id_city')->dropDownList(
                $city,         
                ['prompt'=>'-']
            );
        ?>
        <?= $form->field($model, 'id_area')->dropDownList(
                $area,         
                ['prompt'=>'-']
            );
        ?>
        <?= $form->field($model, 'id_street')->dropDownList(
                $street,         
                ['prompt'=>'-']
            );
        ?>
        <?= $form->field($model, 'house') ?>
        <?= $form->field($model, 'apartment') ?>
        <?= $form->field($model, 'is_map')->checkBox() ?>
    
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- users-address-form-address -->
