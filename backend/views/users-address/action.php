<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Banners $model
 */

$this->title = $model->isNewRecord ? 'Добавить' : 'Сохранить';
$this->params['breadcrumbs'][] = ['label' => 'Адреса'];
if($type == 'address')
	$this->params['breadcrumbs'][] = ['label' => $typeru, 'url' => ['/users-address/'.$type.'/'.$id_user]];
else
	$this->params['breadcrumbs'][] = ['label' => $typeru, 'url' => ['/users-address/'.$type]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banners-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?
    if($type == 'address') {
    	echo $this->render('form/'.$type, [
	        'model' => $model,
	        'data' => $data,
	        'region' => $region,
	        'city' => $city,
	        'area' => $area,
	        'street' => $street,
	        'id_user' => $id_user,
	    ]);
    } else {
    	echo $this->render('form/'.$type, [
	        'model' => $model,
	        'data' => $data,
	    ]);
    }
    ?>

</div>
