<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\BannersSearch $searchModel
 */

$this->title = 'Адреса';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/users']];
$this->params['breadcrumbs'][] = ['label' => $userInfo->uid, 'url' => ['/users/update/'.$userInfo->id]];
$this->params['breadcrumbs'][] = 'Адреса пользователя';

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;

?>
<div class="banners-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить адрес', ['add-address', 'id' => $_GET['id']], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Описание',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->description;
                },
            ],
            [
                'attribute' => 'Регион',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->region->title;
                },
            ],
            [
                'attribute' => 'Город',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->city->title;
                },
            ],
            [
                'attribute' => 'Район',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->area->title;
                },
            ],
            [
                'attribute' => 'Улица',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->street->title;
                },
            ],
            [
                'attribute' => 'Дом',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->house;
                },
            ],
            [
                'attribute' => 'Офис',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->apartment;
                },
            ],
            [
                'attribute' => 'Показывать на карте',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->is_map ? 'да' : 'нет';
                },
            ],
            // 'is_visible:boolean',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                   'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '/users-address/edit-address?id='.$model->id);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '/users-address/delete-address?id='.$model->id.'&parent='.$_GET['id']);
                    }, 
                ],
            ],
        ],
    ]); ?>

</div>
