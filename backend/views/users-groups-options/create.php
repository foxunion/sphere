<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsersGroupsOptions $model
 */

$this->title = 'Добавить опцию';
$this->params['breadcrumbs'][] = ['label' => 'Опции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-groups-options-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
