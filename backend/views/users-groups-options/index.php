<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\UsersGroupsOptionsSearch $searchModel
 */

$this->title = 'Опции';
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;

?>
<div class="users-groups-options-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить опцию', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Пакеты', ['/users-groups'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'desc:ntext',
            'is_visible:boolean',

            ['class' => 'yii\grid\ActionColumn', 
                'template' => '{list} {update} {delete}',
            ],
        ],
    ]); ?>

</div>
