<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\ServicesData $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Services Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_service',
            'id_user',
            'id_unit',
            'avg_price',
            'price',
            'image',
            'worktime',
            'description',
            'discount',
            'is_qualitymark',
            'is_recomendated',
        ],
    ]) ?>

</div>
