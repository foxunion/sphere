<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ServicesData $model
 */

$this->title = 'Добавить услугу';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/users']];
$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['index', 'id' => $_GET['id']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sphere' => $sphere,
        'units' => $units,
    ]) ?>

</div>
