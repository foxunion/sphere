<?php
use yii\helpers\Html;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var app\models\ServicesData $model
 */

$this->title = 'Редактировать услугу: ' . $model->service->title;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/users']];
$this->params['breadcrumbs'][] = ['label' => $model->user->uid];
//, 'url' => ['/users/update', 'id' => $model->idUser->id]];
$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['index', 'id' => $model->user->id]];
$this->params['breadcrumbs'][] = ['label' => $model->service->title];
$this->params['breadcrumbs'][] = 'Редактировать';

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;
?>
<div class="services-data-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sphere' => $sphere,
        'units' => $units,
    ]) ?>

</div>
