<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\TypeOrganization $model
 */

$this->title = 'Добавить тип';
$this->params['breadcrumbs'][] = ['label' => 'Типы организаций', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-organization-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
