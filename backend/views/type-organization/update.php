<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\TypeOrganization $model
 */

$this->title = 'Редактировать тип: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Типы организаций', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="type-organization-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
