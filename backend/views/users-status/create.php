<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsersStatus $model
 */

$this->title = 'Добавить статус';
$this->params['breadcrumbs'][] = ['label' => 'Статусы пользователя', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
