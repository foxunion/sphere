<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?= Html::csrfMetaTags() ?>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'На сайт',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    Yii::$app->user->isGuest ?
                        ['label' => 'Авторизация', 'url' => ['/site/login']] :
                        ['label' => 'Выход (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                ],
            ]);
            NavBar::end();
        ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 col-md-2 sidebar">
            <?php echo Nav::widget([
                'options' => ['class'=>'nav nav-sidebar'],
                'items' => [
                    ['label' => 'Текстовые страницы', 'url' => ['/pages/index']],
                    ['label' => 'Баннеры', 'url' => ['/banners/index']],
                    ['label' => 'Каталог',
                        'items' => [
                            '<li class="divider"></li>',
                            '<li class="dropdown-header">Сферы</li>',
                            ['label' => 'Сферы', 'url' => ['/services-category']],
                            '<li class="divider"></li>',
                            '<li class="dropdown-header">Различные параметры</li>',
                            ['label' => 'Еденицы измерения услуг', 'url' => ['/services-units/index']],
                        ],
                    ],
                    ['label' => 'Адреса',
                        'items' => [
                            ['label' => 'Регион', 'url' => ['/users-address/region']],
                            ['label' => 'Город', 'url' => ['/users-address/city']],
                            ['label' => 'Исторический район', 'url' => ['/users-address/area']],
                            ['label' => 'Улица', 'url' => ['/users-address/street']],
                        ],
                    ],
                    ['label' => 'Пользователи',
                        'items' => [
                            '<li class="divider"></li>',
                            '<li class="dropdown-header">Пакеты услуг</li>',
                            ['label' => 'Пакеты', 'url' => ['/users-groups']],
                            ['label' => 'Опции для пакетов', 'url' => ['/users-groups-options']],
                            ['label' => 'Права на пакеты', 'url' => ['/users-groups-rules']],
                            '<li class="divider"></li>',
                            '<li class="dropdown-header">Управление пользователями</li>',
                            ['label' => 'Список пользователей', 'url' => ['/users']],
                            ['label' => 'Личные сообщения', 'url' => ['/users-message']],
                            '<li class="divider"></li>',
                            '<li class="dropdown-header">Различные параметры</li>',
                            ['label' => 'Статусы пользователей', 'url' => ['/users-status']],
                            ['label' => 'Коды телефонов', 'url' => ['/codes-phones']],
                            ['label' => 'Типы организаций', 'url' => ['/type-organization']],
                        ],
                    ],
                    ['label' => 'Отзывы',
                        'items' => [
                            ['label' => 'Управление отзывами', 'url' => ['/reviews']],
                            ['label' => 'Шаблоны ответов', 'url' => ['/reviews-rating']],
                        ],
                    ],
                    ['label' => 'Настройки сайта', 'url' => ['/site-config']],
                    ['label' => 'Администрация сайта', 'url' => ['/user']],
                   
                ],
            ]);
            ?>
          </div>
          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Панель администратора</h1>
              <?= Breadcrumbs::widget([
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
              ]) ?>
              <?= $content ?>
          </div>
        </div>
      </div>
 
    </div>

   
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
