<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CodesPhones $model
 */

$this->title = 'Добавить код';
$this->params['breadcrumbs'][] = ['label' => 'Коды телефонов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="codes-phones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
