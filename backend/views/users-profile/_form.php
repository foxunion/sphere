<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\imagine\Image;

/**
 * @var yii\web\View $this
 * @var app\models\UsersProfile $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="users-profile-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <!-- <?= $form->field($model, 'id_user')->textInput() ?> -->

    <?= $form->field($model, 'id_status')->dropDownList(
            $usersStatus,         
            ['prompt'=>'-']
        ); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'bonus')->textInput() ?>

    <?= $form->field($model, 'balance')->textInput() ?>

    <?= $form->field($model, 'schedule')->textInput() ?>

    <?= $form->field($model, '_image')->fileInput() ?>
    <?php if($image = $model->getImage()): ?>
        <?php
        $image = $model->getImage()->getUrl('120x120'); ?>
        <img src="<?=$image?>">
        <?= $form->field($model, '_deleteimage')->checkBox() ?>      
    <?php endif; ?>

    <?= $form->field($model, 'url_vk')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'url_www')->textInput(['maxlength' => 45]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
