<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsersProfile $model
 */

$this->title = 'Профиль пользователя';
// $this->params['breadcrumbs'][] = ['label' => 'Users Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'usersStatus' => $usersStatus,
    ]) ?>

</div>
