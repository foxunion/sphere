<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ServicesUnits $model
 */

$this->title = 'Редактирование велечины: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Велечины измерения', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="services-units-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
