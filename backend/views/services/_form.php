<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Services $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="services-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'id_category')->dropDownList(
            $parent,         
            ['prompt'=>'-']
        );
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 128]) ?>

     <?= yii\imperavi\Widget::widget([
            'model' => $model,
            'attribute' => 'description',
            'options' => [
                'lang' => 'ru',
                'imageUpload' => '/site/upload-image',
                'fileUpload' => '/site/upload-file',
            ],
    ]); ?>

    <?= $form->field($model, 'keyword')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, '_image')->fileInput() ?>

    <?= $form->field($model, 'is_visible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
