<?php

use yii\helpers\Html;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var app\models\Services $model
 */

$this->title = 'Редактировать услугу: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;
?>
<div class="services-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'parent' => $parent,
    ]) ?>

</div>
