<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Reviews $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="reviews-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_user')->dropDownList(
            $users,         
            ['prompt'=>'-']
        );
    ?>

    <?= $form->field($model, 'id_service')->dropDownList(
            $services,         
            ['prompt'=>'-']
        );
    ?>

    <?= $form->field($model, 'id_rating')->dropDownList(
            $templateAnswers,         
            ['prompt'=>'-']
        );
    ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_visible')->checkBox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
