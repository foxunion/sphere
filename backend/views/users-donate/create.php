<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsersDonate $model
 */

$this->title = 'Create Users Donate';
$this->params['breadcrumbs'][] = ['label' => 'Users Donates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-donate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
