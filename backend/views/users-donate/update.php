<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UsersDonate $model
 */

$this->title = 'Update Users Donate: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users Donates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="users-donate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
