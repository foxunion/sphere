<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\UsersSearch $searchModel
 */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;

?>
<?= Html::csrfMetaTags() ?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Подсказка</h3>
      </div>
        <div class="panel-body">
            <span class="glyphicon glyphicon-user"></span> - Профиль пользователя<br>
            <span class="glyphicon glyphicon-home"></span> - Адреса пользователя<br>
            <span class="glyphicon glyphicon-phone-alt"></span> - телефоны пользователя<br>
            <span class="glyphicon glyphicon-th"></span> - Услуги пользователя
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'uid',
            'email:email',

            // 'updated',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{profile} {address} {phones} {services} {update} {delete}',
                'buttons' => [
                    'profile' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', '/users-profile/update/'.$model->id);
                    },
                    'address' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-home"></span>', '/users-address/address/'.$model->id);
                    },
                    'phones' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-phone-alt"></span>', '/users-phones/'.$model->id);
                    },
                    'services' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-th"></span>', '/services-data/'.$model->id);
                    },
                   
                ],],
        ],
    ]); ?>

</div>
