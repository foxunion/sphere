<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'components' => [
        'user' => [
            'identityClass' => 'backend\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'image' => [
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD',  //GD or Imagick
        ],
        'security' => [
            'class' => 'yii\base\Security',
        ],
        
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<action:(login|logout|about)>' => 'site/<action>',

                '<controller:(pages)>/<action:(index|create)>' => '<controller>/<action>',
                '<controller:(pages)>/<id:\d+>' => '<controller>/view',
                '<controller:(pages)>/<path:\w+>' => '<controller>/view',
                '<controller:(pages)>/<action>/<id:\d+>' => '<controller>/<action>',
                '<controller:(pages)>/<action>/<path:\w+>' => '<controller>/<action>',
                '<controller:(users-phones|services-data)>/<id:\d+>' => '<controller>/index',
                '<controller>' => '<controller>/index',
                '<controller>/<id:\d+>' => '<controller>/view',
                '<controller>/<action>/<id:\d+>' => '<controller>/<action>',
            ],
        ],
    ],
    'modules' => [
        'yii2images' => [
            'class' => 'rico\yii2images\Module',
            //be sure, that permissions ok 
            //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
            'imagesStorePath' => '@common/../uploads/images/', //path to origin images
            'imagesCachePath' => '@common/../uploads/images/cache', //path to resized copies
            'graphicsLibrary' => 'GD', //but really its better to use 'Imagick' 
            'placeHolderPath' => Yii::getAlias('@common/../uploads/images/placeHolder.png'), // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
        ],
    ],
    'params' => $params,
];
