<?php

namespace backend\controllers;

use Yii;
use common\models\UsersProfile;
use common\models\UsersStatus;
use common\models\search\UsersProfileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
/**
 * UsersProfileController implements the CRUD actions for UsersProfile model.
 */
class UsersProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => true,
                        'verbs' => ['POST']
                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsersProfile models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersProfileSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
         
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single UsersProfile model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UsersProfile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   
        $model = new UsersProfile;
        $usersStatus = ArrayHelper::map(UsersStatus::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'usersStatus' => $usersStatus,
            ]);
        }
    }

    /**
     * Updates an existing UsersProfile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $usersStatus = ArrayHelper::map(UsersStatus::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
             
            return $this->render('update', [
                'model' => $model,
                'usersStatus' => $usersStatus,
            ]);
        }
    }

    /**
     * Deletes an existing UsersProfile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the UsersProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsersProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsersProfile::findOne(['id_user'=>$id])) !== null) {
            return $model;
        } else {
            return  $this->redirect(['create', 'id_user' => $id]);;
        }
    }
}
