<?php

namespace backend\controllers;

use Yii;
use common\models\ServicesCategory;
use common\models\search\ServicesCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ServicescategoryController implements the CRUD actions for ServicesCategory model.
 */
class ServicesCategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => true,
                        'verbs' => ['POST']
                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['get'],
                ],
            ],
        ];
    }

    /**
     * Lists all ServicesCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ServicesCategory;
        return $this->render('index', [
            'model' => $model->find()->where(array('parent_id'=>null))->all()
        ]);
    }

    /**
     * Displays a single ServicesCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ServicesCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ServicesCategory;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
            return $this->redirect(['index']);
        } else {
            $parent = ArrayHelper::map($model::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
            return $this->render('create', [
                'model' => $model,
                'parent' => $parent,
            ]);
        }
    }

    /**
     * Updates an existing ServicesCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
            return $this->redirect(['index']);
        } else {
            $parent = ArrayHelper::map($model::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
            return $this->render('update', [
                'model' => $model,
                'parent' => $parent,
            ]);
        }
    }

    /**
     * Deletes an existing ServicesCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the ServicesCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ServicesCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServicesCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
