<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use common\models\Users;
use common\models\UsersAddress;
use common\models\search\UsersAddressSearch;
use common\models\UsersAddressRegion;
use common\models\search\UsersAddressRegionSearch;
use common\models\UsersAddressCity;
use common\models\search\UsersAddressCitySearch;
use common\models\UsersAddressArea;
use common\models\search\UsersAddressAreaSearch;
use common\models\UsersAddressStreet;
use common\models\search\UsersAddressStreetSearch;
class UsersAddressController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['region', 'addregion', 'editregion', 'deleteregion',
                           'city', 'addcity', 'editcity', 'deletecity',
                           'area', 'addarea', 'editarea', 'deletearea',
                           'street', 'addstreet', 'editstreet', 'deletestreet',
                           'address', 'addaddress', 'editaddress', 'deleteaddress',
                          ],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => true,
                        'verbs' => ['POST']
                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
         
        return $this->render('index');
    }

    public function actionAddress($id) 
    {
        $searchModel = new UsersAddressSearch;
        $userInfo = Users::findOne($id);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
         
        return $this->render('address', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'userInfo' => $userInfo,
        ]);
    }

    public function actionAddAddress() 
    {
        $model = new UsersAddress;
        $region = ArrayHelper::map(UsersAddressRegion::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $city = ArrayHelper::map(UsersAddressCity::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $area = ArrayHelper::map(UsersAddressArea::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $street = ArrayHelper::map(UsersAddressStreet::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->id_user = $_GET['id'];
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['edit-address', 'id' => $model->id]);
            }
        } else {
             
            return $this->render('action', [
                'model' => $model,
                'region' => $region,
                'city' => $city,
                'area' => $area,
                'street' => $street,
                'id_user' => $_GET['id'],
                'data' => '',
                'type' => 'address',
                'typeru' => 'Адреса',
            ]);
        }
    }

    public function actionEditAddress($id)
    {
        $model = UsersAddress::findOne($id);
        $region = ArrayHelper::map(UsersAddressRegion::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $city = ArrayHelper::map(UsersAddressCity::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $area = ArrayHelper::map(UsersAddressArea::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $street = ArrayHelper::map(UsersAddressStreet::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
                return $this->redirect(['address', 'id' => $model->id_user]);
            }
        } else {
             
            return $this->render('action', [
                'model' => $model,
                'region' => $region,
                'city' => $city,
                'area' => $area,
                'street' => $street,
                'id_user' => $model->id_user,
                'data' => '',
                'type' => 'address',
                'typeru' => 'Адреса',
            ]);
        }
    }

    public function actionDeleteAddress($id, $parent) 
    {
        UsersAddress::findOne($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['/users-address/address/'.$parent]);
    }

    /**
     * Регион
     */
    public function actionRegion()
    {
        $searchModel = new UsersAddressRegionSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
         
        return $this->render('region', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionAddregion()
    {
        $model = new UsersAddressRegion;

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());

            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['editregion', 'id' => $model->id]);
            }
        } else {
             
            return $this->render('action', [
                'model' => $model,
                'data' => '',
                'type' => 'region',
                'typeru' => 'Регионы',
            ]);
        }
    }

    public function actionEditregion($id)
    {
        $model = UsersAddressRegion::findOne($id);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
                return $this->redirect(['editregion', 'id' => $model->id]);
            }
        } else {
             
            return $this->render('action', [
                'model' => $model,
                'data' => '',
                'type' => 'region',
                'typeru' => 'Регионы',
            ]);
        }
    }

    public function actionDeleteregion($id)
    {
        UsersAddressRegion::findOne($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['region']);
    }

    /**
     * Город
     */
    public function actionCity()
    {
        $searchModel = new UsersAddressCitySearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
         
        return $this->render('city', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionAddcity()
    {
        $model = new UsersAddressCity;

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['editcity', 'id' => $model->id]);
            }
        } else {
            $data = ArrayHelper::map(UsersAddressRegion::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');

             
            return $this->render('action', [
                'model' => $model,
                'data' => $data,
                'type' => 'city',
                'typeru' => 'Города',
            ]);
        }
    }

    public function actionEditcity($id)
    {
        $model = UsersAddressCity::findOne($id);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
                return $this->redirect(['editcity', 'id' => $model->id]);
            }
        } else {
            $data = ArrayHelper::map(UsersAddressRegion::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
             
            return $this->render('action', [
                'model' => $model,
                'data' => $data,
                'type' => 'city',
                'typeru' => 'Города',
            ]);
        }
    }

    public function actionDeletecity($id)
    {
        UsersAddressCity::findOne($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['city']);
    }

    /**
     * Историеский район
     */
    public function actionArea()
    {
        $searchModel = new UsersAddressAreaSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
         
        return $this->render('area', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionAddarea()
    {
        $model = new UsersAddressArea;

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['editarea', 'id' => $model->id]);
            }
        } else {
            $data = ArrayHelper::map(UsersAddressCity::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
             
            return $this->render('action', [
                'model' => $model,
                'data' => $data,
                'type' => 'area',
                'typeru' => 'Историеский район',
            ]);
        }
    }

    public function actionEditarea($id)
    {
        $model = UsersAddressArea::findOne($id);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
                return $this->redirect(['editarea', 'id' => $model->id]);
            }
        } else {
            $data = ArrayHelper::map(UsersAddressCity::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
             
            return $this->render('action', [
                'model' => $model,
                'data' => $data,
                'type' => 'area',
                'typeru' => 'Историеский район',
            ]);
        }
    }

    public function actionDeletearea($id)
    {
        UsersAddressArea::findOne($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['area']);
    }

    /**
     * Улица
     */
    public function actionStreet()
    {
        $searchModel = new UsersAddressStreetSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
         
        return $this->render('street', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionAddstreet()
    {
        $model = new UsersAddressStreet;
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['editstreet', 'id' => $model->id]);
            }
        } else {
            $data = ArrayHelper::map(UsersAddressArea::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
             
            return $this->render('action', [
                'model' => $model,
                'data' => $data,
                'type' => 'street',
                'typeru' => 'Улицы',
            ]);
        }
    }

    public function actionEditstreet($id)
    {
        $model = UsersAddressStreet::findOne($id);
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
                return $this->redirect(['editstreet', 'id' => $model->id]);
            }
        } else {
            $data = ArrayHelper::map(UsersAddressArea::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
             
            return $this->render('action', [
                'model' => $model,
                'data' => $data,
                'type' => 'street',
                'typeru' => 'Улицы',
            ]);
        }
    }

    public function actionDeletestreet()
    {
        UsersAddressStreet::findOne($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['street']);
    }
}
