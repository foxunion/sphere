<?php

namespace backend\controllers;

use Yii;
use common\models\UsersGroups;
use common\models\UsersGroupsOptions;
use common\models\UsersGroupsOptionRelations;
use common\models\search\UsersGroupsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\helpers\YiiTextHelper;
/**
 * UsersGroupsController implements the CRUD actions for UsersGroups model.
 */
class UsersGroupsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => true,
                        'verbs' => ['POST']
                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsersGroups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersGroupsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
         
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single UsersGroups model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UsersGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UsersGroups;
        $usersGroupsOptionsModel = new UsersGroupsOptions;
        
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save() && isset($_POST['UsersGroups']['options'])) {
                foreach ($_POST['UsersGroups']['options'] as $value) {
                    $UsersGroupsOptionRelations = new UsersGroupsOptionRelations;
                    $UsersGroupsOptionRelations->id_packet = $model->id;
                    $UsersGroupsOptionRelations->id_packet_option = $value;
                    $UsersGroupsOptionRelations->save();
                }
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
             
            return $this->render('create', [
                'model' => $model,
                'usersGroupsOptionsModel' => ArrayHelper::map($usersGroupsOptionsModel->find()->select('id, title')->all(), 'id', 'title'),
            ]);
        }
    }


    /**
     * Updates an existing UsersGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $usersGroupsOptionsModel = new UsersGroupsOptions;
        $UsersGroupsOptionRelations = new UsersGroupsOptionRelations;

        $selectOptions = $UsersGroupsOptionRelations->find()->select('id_packet_option')->where('id_packet = :id_packet', ['id_packet'=>$id])->all();
        foreach ($selectOptions as $value) {
            $options[] = $value->id_packet_option;
        }
        $model->options = $options;

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save() && isset($_POST['UsersGroups']['options'])) {
                UsersGroupsOptionRelations::deleteAll('id_packet = ' . $id);
                foreach ($_POST['UsersGroups']['options'] as $value) {
                    $UsersGroupsOptionRelations = new UsersGroupsOptionRelations;
                    $UsersGroupsOptionRelations->id_packet = $model->id;
                    $UsersGroupsOptionRelations->id_packet_option = $value;
                    $UsersGroupsOptionRelations->save();
                }

                Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
             
            return $this->render('update', [
                'model' => $model,
                'usersGroupsOptionsModel' => ArrayHelper::map($usersGroupsOptionsModel->find()->select('id, title')->all(), 'id', 'title'),
            ]);
        }
    }

    /**
     * Deletes an existing UsersGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the UsersGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsersGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsersGroups::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
