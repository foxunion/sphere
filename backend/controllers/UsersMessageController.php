<?php

namespace backend\controllers;

use Yii;
use common\models\Users;
use common\models\UsersMessage;
use common\models\search\UsersMessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * UsersMessageController implements the CRUD actions for UsersMessage model.
 */
class UsersMessageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => true,
                        'verbs' => ['POST']
                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsersMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersMessageSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
         
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single UsersMessage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UsersMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UsersMessage;

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->id_sender = 1; //admin
            $model->status = 0; //0 - не прочитаное, 1 - прочитаное, 2 - удаленное
            $model->type = 0; //0 - системное сообщение, 1 - обычное сообщение
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            $users = ArrayHelper::map(Users::find()->select('id, uid')->all(), 'id', 'uid');
             
            return $this->render('create', [
                'model' => $model,
                'users' => $users,
            ]);
        }
    }

    /**
     * Updates an existing UsersMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->id_sender = 1; //admin
            $model->status = 0; //0 - не прочитаное, 1 - прочитаное, 2 - удаленное
            $model->type = 0; //0 - системное сообщение, 1 - обычное сообщение
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            $users = ArrayHelper::map(Users::find()->select('id, uid')->all(), 'id', 'uid');
             
            return $this->render('update', [
                'model' => $model,
                'users' => $users,
            ]);
        }
    }

    /**
     * Deletes an existing UsersMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the UsersMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsersMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsersMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
