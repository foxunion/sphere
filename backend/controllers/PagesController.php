<?php

namespace backend\controllers;

use Yii;
use common\models\Pages;
use common\models\search\PagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\helpers\YiiTextHelper;
/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => true,
                        'verbs' => ['POST']
                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id=null, $path=null)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $path),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pages;

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->path == '')
                $model->path = YiiTextHelper::translite(Yii::$app->request->post()['Pages']['title']);
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            $parent = ArrayHelper::map($model::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
            return $this->render('create', [
                'model' => $model,
                'parent' => $parent,
            ]);
        }
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->path == '')
                $model->path = YiiTextHelper::translite(Yii::$app->request->post()['Pages']['title']);
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            $parent = ArrayHelper::map($model::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
            return $this->render('update', [
                'model' => $model,
                'parent' => $parent,
            ]);
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id=null, $path=null)
    {
        if ($id) {
            $model = Pages::find()->where(['id'=>$id])->one();
        } elseif ($path) {
            $model = Pages::find()->where(['path'=>$path])->one();
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
