<?php

namespace backend\controllers;

use Yii;
use common\models\Services;
use common\models\ServicesCategory;
use common\models\search\ServicesSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * ServicesController implements the CRUD actions for Services model.
 */
class ServicesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => true,
                        'verbs' => ['POST']
                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Services models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServicesSearch;
        $servicesCategory = '';

        if(isset($_GET['id'])) {
            $servicesCategoryModel = new ServicesCategory;
            $servicesCategory = $servicesCategoryModel->findOne($_GET['id']);
        } else {
            return $this->redirect('/services-category');
        }

        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
         
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'servicesCategory' => $servicesCategory
        ]);
    }

    /**
     * Displays a single Services model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Services model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Services;
        if(isset($_GET['id']))
            $model->id_category = $_GET['id'];
        $ServicesCategory = new ServicesCategory;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            $parent = ArrayHelper::map($ServicesCategory::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
             
            return $this->render('create', [
                'model' => $model,
                'parent' => $parent,
            ]);
        }
    }

    /**
     * Updates an existing Services model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $ServicesCategory = new ServicesCategory;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            $parent = ArrayHelper::map($ServicesCategory::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
             
            return $this->render('update', [
                'model' => $model,
                'parent' => $parent,
            ]);
        }
    }

    /**
     * Deletes an existing Services model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Services model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Services the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Services::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
