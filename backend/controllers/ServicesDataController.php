<?php

namespace backend\controllers;

use Yii;
use common\models\ServicesData;
use common\models\ServicesUnits;
use common\models\Services;
use common\models\Users;
use common\models\search\ServicesDataSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ServicesDataController implements the CRUD actions for ServicesData model.
 */
class ServicesDataController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => true,
                        'verbs' => ['POST']
                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ServicesData models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $userInfo = Users::findOne($id);
        $searchModel = new ServicesDataSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
         
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'userInfo' => $userInfo,
        ]);
    }

    /**
     * Displays a single ServicesData model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ServicesData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ServicesData;

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->id_user = $_GET['id'];
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            $sphere = ArrayHelper::map(Services::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
            $units = ArrayHelper::map(ServicesUnits::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
             
            return $this->render('create', [
                'model' => $model,
                'sphere' => $sphere,
                'units' => $units,
            ]);
        }
    }

    /**
     * Updates an existing ServicesData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            $sphere = ArrayHelper::map(Services::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
            $units = ArrayHelper::map(ServicesUnits::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
             
            return $this->render('update', [
                'model' => $model,
                'sphere' => $sphere,
                'units' => $units,
            ]);
        }
    }

    /**
     * Deletes an existing ServicesData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['/users']);
    }

    /**
     * Finds the ServicesData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ServicesData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServicesData::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
