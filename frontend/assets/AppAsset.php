<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        "js/chosen/chosen.min.css",
        "js/jquery-ui-1.10.4.custom.css",
        "js/pickmeup/pickmeup.css",
        'css/style.css',
    ];
    public $js = [
        //"js/jquery-1.11.1.min.js",
        "js/chosen/chosen.jquery.min.js",
        "js/pickmeup/jquery.pickmeup.js",
        "js/jquery.bpopup.min.js",
        "js/jquery-ui-1.10.4.custom.min.js",
        "js/parallax/jquery.parallax.js",
        //"js/dependent-dropdown.min.js",
        "js/script.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
