<?php

namespace frontend\widgets;
use common\models\SiteConfig;

class EchoStatic extends \yii\bootstrap\Widget
{
    public $alias = '';

    public function init()
    {
        parent::init();
        $model = SiteConfig::find()->where(['alias'=>$this->alias])->one();
        if ($model !== null) {
            echo $model->content;
        } else {
            echo "";
        }
    }
}
