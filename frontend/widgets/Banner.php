<?php

namespace frontend\widgets;
use common\models\Banners;

class Banner extends \yii\bootstrap\Widget
{
    public $position = '';

    public function init()
    {
        parent::init();
        $model = Banners::find()->where(['place'=>$this->position])->one();
        if ($model !== null && $model->getImage()!==null ) {
            $text = '<a class="banner" href='.$model->reference.' data-tooltip="'.$model->title.'"><img src="'.$model->getImage()->getUrl('950x150').'"></a>';
            echo $text;
        } else {
            echo "";
        }
    }
}
