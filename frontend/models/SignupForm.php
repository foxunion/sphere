<?php
namespace frontend\models;

use common\models\Users;
use common\models\UsersProfile;
use common\models\UsersPhones;
use common\models\UsersAddress;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $id_user = null;
    public $username;
    public $password;
    public $password_repeat;
    public $email;
    public $_image;
    public $phone;
    public $id_codePhone;
    public $id_usersStatus;
    public $id_typeOrganization;
    public $url_vk;
    public $url_site;
    public $schedule;
    public $onmap;
    public $captcha;
    public $address = array();


    public function attributeLabels()
    {
        return [
            'username' => 'Введите название',
            'password' => 'Пароль',
            'password_repeat' => 'Повторите пароль:',
            'email' => 'Email',
            'phone' => 'Телефон',
            'id_codePhone' => 'Код телефона',
            '_image' => 'Логотип',
            'id_usersStatus' => 'Выберите статус',
            'id_typeOrganization' => 'Тип организации',
            'url_vk' => 'Введите адрес группы',
            'url_site' => 'Введите адрес сайта',
            'schedule' => 'Время работы',
            'onmap' => 'Показать себя на карте',
            'captcha' => 'Введите код с изображения:',
        ];
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter',  'filter' => 'trim'],
            ['username', 'required', 'message' => 'Заполните название организации.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'on'=>"signup", 'targetClass' => '\common\models\Users', 'message' => 'This email address has already been taken.'],

            ['password', 'required', 'on'=>"signup"],
            ['password', 'string', 'min' => 6],

            ['phone', 'required'],
            ['id_codePhone', 'required'],

            ['password_repeat', 'required', 'on'=>"signup" , 'message' => 'Повторите пароль.'],
            ['password_repeat', function ($attribute, $params) {
                if($this->password_repeat != $this->password)
                    $this->addError($attribute, 'Пароли не совпадают.');
            }, 'message' => 'Пароли не совпадают.'],



            ['id_usersStatus', 'required', 'message' => 'Пожалуйста, выберите свой статус.'],
            ['id_typeOrganization', 'required', 'message' => 'Выберите тип организации.'],

            ['captcha', 'required', 'message' => 'Введите код с изображения.'],
            ['captcha', 'captcha'],
            [[  'id_user',
                'username',
                'password',
                'password_repeat',
                'email',
                '_image',
                'phone',
                'id_codePhone',
                'id_usersStatus',
                'id_typeOrganization',
                'url_vk',
                'url_site',
                'schedule',
                'onmap',
                'address'
                ],"safe"],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $this->setScenario('signup');
        $this->attributes = Yii::$app->request->post('SignupForm');

        if ($this->validate()) {
            $user = new Users();
            $user->uid = $this->username;
            $user->email = $this->email;
            $user->new_password = $this->password;
            $user->save();


            $profile = new UsersProfile();
            $profile->id_user = $user->id;
            $profile->id_status = $this->id_usersStatus;
            $profile->id_type = $this->id_typeOrganization;
            $profile->title = $this->username;
            $profile->url_vk = $this->url_vk;
            $profile->url_www = $this->url_site;
            $profile->schedule = $this->schedule;
            $profile->save();
            $profile->_image = $this->_image;
            $profile->update();


            $phone = new UsersPhones();
            $phone->id_user = $user->id;
            $phone->phone = $this->phone;
            $phone->id_code = $this->id_codePhone;
            $phone->save();

            foreach ($this->address as $key => $value) {
                if($value['home']!=''){
                    $address = new UsersAddress();
                    $address->id_user = $user->id;
                    $address->id_region = $this->address[$key]['id_region'];
                    $address->id_city = $this->address[$key]['id_city'];
                    $address->id_street = $this->address[$key]['id_street'];
                    $address->id_area = $this->address[$key]['id_area'];
                    $address->house = $this->address[$key]['home'];
                    $address->apartment = $this->address[$key]['apartment'];
                    $address->is_map = $this->onmap;
                    $address->save();

                }
            }
            return $user;
        }

        return null;
    }

    public function update($id){
        $this->attributes = Yii::$app->request->post('SignupForm');

        if ($this->validate()) {
            
            $user = Users::findOne(['id_user'=>Yii::$app->user->id]);
            $user->uid = $this->username;
            $user->email = $this->email;
            if($this->password!='')
                $user->new_password = $this->password;

            $profile = UsersProfile::findOne(['id_user'=>Yii::$app->user->id]);
            $profile->id_status = $this->id_usersStatus;
            $profile->id_type = $this->id_typeOrganization;
            $profile->title = $this->username;
            $profile->url_vk = $this->url_vk;
            $profile->url_www = $this->url_site;
            $profile->schedule = $this->schedule;
            $profile->_image = $this->_image;

            $phone = UsersPhones::findOne(['id_user'=>Yii::$app->user->id]);
            $phone->phone = $this->phone;
            $phone->id_code = $this->phone;

            foreach ($this->address as $key => $value) {
                if($value[$key]['home']!=''){
                    if(!$address = UsersAddress::findOne($this->address[$key]['id'])){
                        $address = new UsersAddress();
                        $address->id_user = $user->id;
                    }
                    $address = UsersAddress::findOne($this->address[$key]['id']);
                    $address->id_region = $this->address[$key]['id_region'];
                    $address->id_city = $this->address[$key]['id_city'];
                    $address->id_street = $this->address[$key]['id_street'];
                    $address->id_area = $this->address[$key]['id_area'];
                    $address->house = $this->address[$key]['home'];
                    $address->apartment = $this->address[$key]['apartment'];
                    $address->is_map = $this->onmap;

                    $address->update();
                }
            }
            
            $phone->update();
            $profile->update();
            $user->update();

            return $user;
        }
        return null;
    }

    public static function getForm($id)
    {
        $model = new SignupForm();
        
        $user = Users::findOne(Yii::$app->user->id);
        $profile = UsersProfile::findOne(['id_user'=>Yii::$app->user->id]);
        $phone = UsersPhones::findOne(['id_user'=>Yii::$app->user->id]);
        
        $model->username = $profile->title;
        $model->password = "";
        $model->password_repeat = "";
        $model->email = $user->email;
        
        $model->_image = $profile->_image;
        $model->phone = @$phone->phone;
        $model->id_codePhone = @$phone->id_code;
       
        $model->id_usersStatus = $profile->id_status;
        $model->id_typeOrganization = $profile->id_type;
        $model->url_vk = $profile->url_vk;
        $model->url_site = $profile->url_www;
        $model->schedule = $profile->schedule;

        $address = UsersAddress::find()
            ->where(['id_user' => Yii::$app->user->id])
            ->orderBy('id')
            ->all();
        foreach ($address as $key => $value) {
            $model->address[$key]['id'] = $value->id;
            $model->address[$key]['id_region'] = $value->id_region;
            $model->address[$key]['id_city'] = $value->id_city;
            $model->address[$key]['id_street'] = $value->id_street;
            $model->address[$key]['id_area'] = $value->id_area;
            $model->address[$key]['apartment'] = $value->apartment;
        }
        
        return $model;
    }
}
