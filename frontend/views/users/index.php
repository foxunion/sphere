<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\UsersSearch $searchModel
 */

$this->title = 'Организации';
$this->params['breadcrumbs'][] = $this->title;

?>
<h2>Список организаций</h2>
<div class="organizations">
    <div class="col50">
        <div class="group">
            <?php $centerok = false;?>
            <?php foreach ($data as $key => $value):?>
                    <?php if($key == 0):?>
                        <div class="char"><?= mb_substr($value->title,0,1,'utf-8');?></div>
                    <?php else:
                        if( mb_substr($data[$key-1]->title,0,1,'utf-8') != mb_substr($value->title,0,1,'utf-8')):?>
                            </div>
                            <?php if($key >= count($data)/2 && !$centerok):?>
                                </div>
                                <div class="col50">
                                <?php $centerok=true;?>
                            <?php endif;?>
                            <div class="group">
                            <div class="char"><?= mb_substr($value->title,0,1,'utf-8');?></div>
                        <?php endif;
                    endif;?>
                    <div class="item">
                        <a href="/users/<?=$value->user->id?>"><?=$value->title?></a>
                    </div>
            <?php endforeach;?>
        </div>
    </div>
    <div class="clear"></div>
</div>