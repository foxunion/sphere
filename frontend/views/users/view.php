<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */

$this->title = $model->usersProfile->title;
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->usersProfile->title;
?>
<div class="organization">
        <h4><?=$model->usersProfile->typeOrganization->value?> "<?=$model->usersProfile->title?>"</h4>
        <div class="left">
            <img src="<?=$model->usersProfile->getImage()->getUrl('120x120') ?>" alt="<?= Html::encode($this->title) ?>">
        </div>
        <div class="right">
            <?php foreach ($model->usersPhones as $key => $Phone) { ?>
            <div class="item">
                <i class="icon"></i>
                <span class="value"><?=$Phone->text;?></span>
            </div>
            <?php } ?>

            <?php foreach ($model->usersAddresses as $key => $Address) { ?>
                <div class="item" data-address="<?=$Address->geoText?>" data-address-name="<?=$Address->description?>" data-name="<?=$model->usersProfile->title?>">
                    <i class="icon"></i>
                    <span class="value"><?=$Address->text?></span>
                </div>
            <?php } ?>

            <div class="item">
                <i class="icon"></i>
                <span class="title">E-mail:</span>
                <span class="value"><a href="mailto:<?=$model->email;?>"><?=$model->email;?></a></span>
            </div>
            <div class="item status">
                <i class="icon"></i>
                <span class="title">Статус:</span>
                <span class="value marked"><?=$model->usersProfile->status->title;?></span>
            </div>
            <div class="item">
                <i class="icon"></i>
                <span class="title">Сайт:</span>
                <span class="value"><a href="http://<?=$model->usersProfile->url_www;?>"><?=$model->usersProfile->url_www;?></a></span>
            </div>
            <div class="item">
                <i class="icon"></i>
                <span class="title">Vk.com: </span>
                <a href="https://vk.com/<?=$model->usersProfile->url_vk;?>">
                    <span class="value"><?=$model->usersProfile->url_vk;?></span>
                </a>
            </div>
            <div class="item">
                <i class="icon"></i>
                <span class="title">Режим работы:</span>
                <span class="value"><?=$model->usersProfile->schedule;?></span>
            </div>
            <div class="item">
                <i class="icon"></i>
                <span class="title">Дата последнего обновления:</span>
                <span class="value"><?php
                    $date = new DateTime($model->updated_at);
                    echo $date->format('d.m.Y');
                    ?>
                    </span>
            </div>
        </div>
    <div class="clear"></div>
    <div id="map" style="width: 100%; height: 400px"></div>
    <div class="clear"></div>
</div>
<?php if($model->servicesDatas) {?>
<div class="search">
    <h3>Предложения от организации</h3>
    <div class="items">
      <?php
      foreach ($model->servicesDatas as $key => $data)
          echo $this->render('/services-data/_view',array(
              'data'=>$data,
          ));
      ?>
    </div>
</div>
<?php } else { ?>
  <p>Нет предложений от этой организации</p>
<?php }?>
    <h3>Отзывы</h3>

<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>
    function addPointToMap(map, item){
        var address = item.attr('data-address');
        var balloon = '';
        balloon += item.attr('data-name');
        balloon += '<br/>';
        balloon += item.attr('data-address-name');
        balloon += '<br/>';
        balloon += item.attr('data-address');
        var myGeocoder = ymaps.geocode(address);
        myGeocoder.then(
            function (res) {
                myGeoObject = new ymaps.GeoObject({
                        geometry: {
                            type: "Point",// тип геометрии - точка
                            coordinates: res.geoObjects.get(0).geometry.getCoordinates() // координаты точки
                        },
                        properties: {
                          balloonContentBody: balloon
                        }
                    });
                map.geoObjects.add(myGeoObject); // Размещение геообъекта на карте.
            },
            function (err) {
                cosole.log('Ошибка');
            }
        );
    }
    ymaps.ready(function () {
        var myMap = new ymaps.Map("map", {
              center: [56.009286, 92.872232],
              zoom: 13,
              controls: []
            });
        $(".item[data-address]").each(function(index, val) {
            addPointToMap(myMap,$(this));
        });
    });
</script>