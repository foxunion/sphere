<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\UsersMessage $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="users-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'id_sender')->textInput() ?> -->
    <?= $form->field($model, 'id_recipient')->dropDownList(
            $users,         
            ['prompt'=>'-']
        );
    ?>

    <!-- <?= $form->field($model, 'type')->textInput() ?> -->

    <!-- <?= $form->field($model, 'status')->textInput() ?> -->

    <?= $form->field($model, 'subject')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
