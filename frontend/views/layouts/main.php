<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use frontend\widgets\EchoStatic;
use frontend\widgets\Banner;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?/*= Html::csrfMetaTags()*/ ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <ul id="scene">
        <?php
            $rounds = 20;
        ?>
      <li class="layer" data-depth="0.20">
          <?php for ($i = 1; $i <= $rounds; $i++) {
                echo "<div></div>";
            }?>
      </li>
      <li class="layer" data-depth="0.40">
        <?php for ($i = 1; $i <= $rounds; $i++) {
            echo "<div><img src='/images/background_icons/".$i.".png'></div>";
        }
        ?>
        </li>
      <li class="layer" data-depth="0.60">
        <?php for ($i = $rounds; $i <= $rounds*2; $i++) {
            echo "<div><img src='/images/background_icons/".$i.".png'></div>";
        }
        ?>
      </li>
      <li class="layer" data-depth="0.80">
          <?php for ($i = 1; $i <= $rounds; $i++) {
                echo "<div></div>";
            }?>
      </li>
    </ul>
    <div class="container">
        <div id="wrap">
            <?= EchoStatic::widget([
                'alias' => "topmenu",
            ]) ?>
            <header id="header">
                <a href="/" class="logo"></a>
                <form action="" class="search">
                    <input type="text" placeholder="Поиск среди 120 000 услуг...">
                    <input type="submit" value="">
                </form>
                <a href="" class="request">
                    <span class="icon"></span>
                    <span class="text">Предложить свои услуги</span>
                </a>
            </header>
            <div id="main">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <div class="clearfix"></div>
                <?= Alert::widget() ?>
                <?= $content ?>
                <?= Banner::widget([
                    'position' => "bottom",
                ]) ?>
            </div>
            <footer id="footer">
                <div class="information">
                    <?= EchoStatic::widget([
                        'alias' => "phone",
                    ]) ?>
                    
                    <?= EchoStatic::widget([
                        'alias' => "bottommenu",
                    ]) ?>
                    
                    <?= EchoStatic::widget([
                        'alias' => "social",
                    ]) ?>
                </div>
                <div class="copywrong">
                    <span class="copy">&copy; <?= date('Y') ?> Сфера</span>
                    <span class="developers">Сайт разработан студией «<a href="http://foxunion.ru" target="_blank">FoxUnion</a>»<span>
                </div>
            </footer>
        </div>
    </div>
    
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
