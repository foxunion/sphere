<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\imagine\Image;

/**
 * @var yii\web\View $this
 * @var app\models\ServicesData $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="services-data-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'id_service')->dropDownList(
            $sphere,         
            ['prompt'=>'-']
        );
    ?>

    <?= $form->field($model, 'id_unit')->dropDownList(
            $units,         
            ['prompt'=>'-']
        );
    ?>

    <?= $form->field($model, 'discount')->textInput() ?>

    

    <?= $form->field($model, 'worktime')->textInput() ?>

    <?= $form->field($model, 'avg_price')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => 45]) ?>

    <label>Описание</label>
    <?= yii\imperavi\Widget::widget([
            'model' => $model,
            'attribute' => 'description',
            'options' => [
                'lang' => 'ru',
                'imageUpload' => '/site/upload-image',
                'fileUpload' => '/site/upload-file',
            ],
    ]); ?>

    <?= $form->field($model, '_image')->fileInput() ?>

    <?php if($model->image): ?>
        <?php
        $file = 'images/services-data/thumb/'.$model->id.'-'.md5($model->id.$model->id_user).'.png';
        if(!file_exists($file))
            Image::thumbnail($model->image, 200, 200)->save($file, ['quality' => 50]); ?>
        <img src="/<?=$file?>">
        <?= $form->field($model, '_deleteimage')->checkBox() ?>      
    <?php endif; ?>

    <?= $form->field($model, 'is_qualitymark')->checkBox() ?>

    <?= $form->field($model, 'is_recomendated')->checkBox() ?>

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
