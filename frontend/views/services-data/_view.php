
<div class="item colored">
    <div class="image">
        <img src="<?=$data->getImage()->getUrl('115x115');?>" alt="<?=$data->service->title;?>">
    </div>
    <aside class="right">
        <?if($data->price){ ?>
            <span class="price"><?=$data->price;?> руб.</span>
        <?} elseif ($data->avg_price) { ?>
            <span class="price">от <?=$data->avg_price;?> руб.</span>
        <?}?>
        <div class="stars">
            <div class="progress" style="width: 100%;"></div>
        </div>

        <span class="reviews">
            <i class="review"><img src="/images/icons/feedback.png"></i>
            <a href="#">
                Отзывы (<span class="numbr">6</span>)
            </a>
        </span>
        
        <?if($data->is_qualitymark){ ?>
            <i class="quality_mark"><img src="/images/icons/redstar.png"></i>
        <? } ?>

        <?if($data->is_recomendated){ ?>
            <span class="featured">
                Рекомендуем
            </span>
        <? } ?>

        <div class="service">
                <i class="up" data-tooltip="Поднять" ></i>
                <i class="online " data-tooltip="Приму заказ онлайн"></i>
                <i class="sale <?if(!$data->discount) echo 'off';?>" data-tooltip="Скидка <?if($data->discount) echo $data->discount."%";?>"></i>
                <i class="time" data-tooltip="Время?"></i>
        </div>
    </aside>
    <div class="info">
        <a class="title" href="/services/<?=$data->service->id?>"><?=$data->service->title;?></a><span class="icon photo"></span><span class="icon price"></span>
        <span class="organization">от <a href="/users/<?=$data->user->id?>"><span class="type"><?=$data->user->usersProfile->typeOrganization->value;?></span> <span class="title">"<?=$data->user->usersProfile->title;?>"</span></a> | <a class="city" href="">Красноярск</a><span class="icon light green"></span></span>
        <span class="description">
            <?=$data->description;?>
        </span>
        <button>Показать контакты</button> <a href="#" class="more">Подробнее</a>
    </div>
</div>