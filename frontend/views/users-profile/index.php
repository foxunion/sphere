<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\UsersProfileSearch $searchModel
 */

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;

?>
    <div class="personal">
                    <h1>Личный кабинет (<?=@$typeOrganization[$profile->id_type]?> "<?=$profile->title?>")</h1>
                    <div class="information">
                        <div class="column50">
                            <div class="item">
                                <span class="title">Номер ID:</span>
                                <span class="value">A344</span>
                            </div>
                            <div class="item">
                                <span class="title">Выбранный пакет опций:</span>
                                <span class="value marked">Оптимум</span>
                                <span class="separator">|</span>
                                <a href="" class="link">Изменить</a>
                            </div>
                            <div class="item">
                                <span class="title">Текущий баланс:</span>
                                <span class="value marked">600руб.</span>
                                <span class="separator">|</span>
                                <a href="" class="link">Пополнить</a>
                            </div>
                            <div class="item">
                                <span class="title">Бонусы:</span>
                                <span class="value">3шт.</span>
                            </div>
                            <div class="item">
                                <span class="title">До окончани демо-решима:</span>
                                <span class="value">28 дней.</span>
                            </div>
                            <div class="item">
                                <span class="title">Доступные пакеты опций:</span>
                                <span class="value">Стандарт, VIP</span>
                            </div>
                        </div>
                        <div class="col50">
                            <div class="item">
                                <span class="title">Просмотров за сутки:</span>
                                <span class="value">3</span>
                            </div>
                            <div class="item status">
                                <span class="title">Текущий статус:</span>
                                <span class="value marked">Активен <span class="light green"></span></span>
                            </div>
                            <div class="item rating">
                                <span class="title">Текущий рейтинг:</span>
                                <span class="value">3.223 (15/48)</span>
                                <div class="stars">
                                    <div class="progress" style="width: 90%;"></div>
                                </div>
                            </div>
                            <div class="item">
                                <span class="title">Дата последнего обновления:</span>
                                <span class="value">12.09.2014</span>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="buttons">
                        <a href="" class="button red statistic">Смотреть статистику</a>
                        <a href="" class="button red up">Поднять аккаунт</a>
                        <a href="" class="button red ad">Недорогая реклама</a>
                        <a href="/logout" class="button red">Выход</a>
                    </div>
                    <div class="clear"></div>
                    <div class="tabs">
                        <menu class="menu">
                            <li>
                                <a href="" rel="messages"><span>Сообщения<br>(2 новых)</span></a>
                            </li>
                            <li>
                                <a href="" rel="information"><span>Общая информация</span></a>
                            </li>
                            <li class="active">
                                <a href="" rel="services"><span>Мои услуги</span></a>
                            </li>
                            <li>
                                <a href="" rel="advanced"><span>Дополнительные возможности</span></a>
                            </li>
                            <li>
                                <a href="" rel="vip"><span>Vip-услуги</span></a>
                            </li>
                            <li>
                                <a href="" rel="packets"><span>Пакеты опций</span></a>
                            </li>
                        </menu>
                        <div class="sections">
                            <section class="tab" data-tab="messages">
                                <div class="left">
                                    <button class="send">Написать письмо</button>
                                    <menu class="menu">
                                        <li class="active"><a href="">Входящие (2)</a></li>
                                        <li><a href="">Корзина</a></li>
                                        <li><a href="">Архив</a></li>
                                        <li><a href="">Спам</a></li>
                                        <li><a href="">Черновики</a></li>
                                    </menu>
                                </div>
                                <div class="right">
                                    <form class="send">
                                        <div class="row to">
                                            <label for="to" class="Messages[to]">Кому:</label>
                                            <input type="text" name="Messages[to]" value="Администратор сайта">
                                        </div>
                                        <div class="row">
                                            <textarea class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex asperiores, itaque iure nemo, quaerat consectetur molestiae quo laboriosam voluptates vel, provident obcaecati quidem! Labore deleniti, recusandae eos consequuntur porro ad!</textarea>
                                        </div>
                                        <div class="buttons">
                                            <button>Отправить</button>
                                            <button><span class="icon delete"></span>Удалить</button>
                                            <button>В архив</button>
                                            <button>Добавить изображение</button>
                                        </div>
                                    </form>
                                    <div class="message">
                                        <span class="from">От кого: Администрация сайта</span>
                                        <span class="date">Получено: 12.08.2014</span>
                                        <div class="text">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, cum fugit repellendus magnam tempora dignissimos! Facere ipsam quia possimus harum. Earum quibusdam maiores tempora optio est modi fuga dolore nihil.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum optio minima hic ea voluptatibus nam rem explicabo debitis, velit at blanditiis enim officia saepe nihil aliquid sed veniam provident mollitia.</p>
                                        </div>
                                        <div class="buttons">
                                            <button>Ответить</button>
                                            <button><span class="icon delete"></span>Удалить</button>
                                            <button>В архив</button>
                                        </div>
                                    </div>
                                    <ul class="items">
                                        <li class="new"><a href="">
                                            <span class="from">От кого: Администрация сайта</span>
                                            <span class="date">Получено: 12.08.2014</span>
                                        </a></li>
                                        <li class="new"><a href="">
                                            <span class="from">От кого: Администрация сайта</span>
                                            <span class="date">Получено: 12.08.2014</span>
                                        </a></li>
                                        <li><a href="">
                                            <span class="from">От кого: Администрация сайта</span>
                                            <span class="date">Получено: 12.08.2014</span>
                                        </a></li>
                                        <li><a href="">
                                            <span class="from">От кого: Администрация сайта</span>
                                            <span class="date">Получено: 12.08.2014</span>
                                        </a></li>
                                        <li><a href="">
                                            <span class="from">От кого: Администрация сайта</span>
                                            <span class="date">Получено: 12.08.2014</span>
                                        </a></li>
                                    </ul>
                                </div>
                            </section>
                            <section class="tab" data-tab="information">
                                <?php echo $this->render('/site/signupForm', [
                                        'model' => $profileform,
                                        'typeOrganization' =>  $typeOrganization,
                                        'codesPhones' =>  $codesPhones,
                                        'usersStatus' =>  $usersStatus,
                                        'addressRegion' =>  $addressRegion,
                                        'addressCity' =>  $addressCity,
                                        'addressArea' =>  $addressArea,
                                        'addressStreet' =>  $addressStreet,
                                    ]);?>
                               
                            </section>
                            <section class="tab" data-tab="services">
                                <form action="">
                                    <div class="categories">
                                        <div class="category">
                                            <div class="info">
                                                <div class="icon"></div>
                                                <span class="title">Швейные услуги, ателье</span>
                                            </div>
                                            <a href="" class="addphoto">Добавить фотографии</a>
                                            <div class="services">
                                                <header>
                                                    <div class="title">Название услуги:</div>
                                                    <div class="price">Стоимость от:</div>
                                                    <div class="price accurate">Точная стоимость:</div>
                                                    <div class="unit">Единицы изм.:</div>
                                                    <div class="image">Фотография:</div>
                                                    <div class="advanced">Доп. опции:</div>
                                                </header>
                                                <div class="items">
                                                    <div class="item">
                                                        <div class="title">
                                                            <span class="icon"></span>
                                                            <input id="service1" type="checkbox"><label for="service1">Пошив одежды</label>
                                                        </div>
                                                        <div class="price">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="price accurate">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="unit">
                                                            <select name="" id="">
                                                                <option value="">- Выберите один из вариантов -</option>
                                                                <option value="0">гривны</option>
                                                                <option value="1">кучки</option>
                                                                <option value="2">грошики</option>
                                                                <option value="3">метры</option>
                                                                <option value="4">штуки</option>
                                                                <option value="5">нарды</option>
                                                            </select>
                                                        </div>
                                                        <div class="image">
                                                            <a href="" class="fancybox img"></a>
                                                            <a href="">Загрузить</a>
                                                        </div>
                                                        <div class="advanced">
                                                            <input type="checkbox" id="advanced01" checked><label for="advanced01"></label>
                                                            <input type="checkbox" id="advanced02" checked><label for="advanced02"></label>
                                                            <input type="checkbox" id="advanced03" checked><label for="advanced03"></label>
                                                            <input type="checkbox" id="advanced04" checked><label for="advanced04"></label>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="title">
                                                            <span class="icon"></span>
                                                            <input id="service2" type="checkbox"><label for="service2">Снятие размеров</label>
                                                        </div>
                                                        <div class="price">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="price accurate">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="unit">
                                                            <select name="" id="">
                                                                <option value="">- Выберите один из вариантов -</option>
                                                                <option value="0">гривны</option>
                                                                <option value="1">кучки</option>
                                                                <option value="2">грошики</option>
                                                                <option value="3">метры</option>
                                                                <option value="4">штуки</option>
                                                                <option value="5">нарды</option>
                                                            </select>
                                                        </div>
                                                        <div class="image">
                                                            <a href="" class="fancybox img"></a>
                                                            <a href="">Загрузить</a>
                                                        </div>
                                                        <div class="advanced">
                                                            <input type="checkbox" id="advanced11"><label for="advanced11"></label>
                                                            <input type="checkbox" id="advanced12"><label for="advanced12"></label>
                                                            <input type="checkbox" id="advanced13" checked><label for="advanced13"></label>
                                                            <input type="checkbox" id="advanced14" checked><label for="advanced14"></label>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="title">
                                                            <span class="icon"></span>
                                                            <input id="service3" type="checkbox"><label for="service3">Распечатка выкройки</label>
                                                        </div>
                                                        <div class="price">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="price accurate">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="unit">
                                                            <select name="" id="">
                                                                <option value="">- Выберите один из вариантов -</option>
                                                                <option value="0">гривны</option>
                                                                <option value="1">кучки</option>
                                                                <option value="2">грошики</option>
                                                                <option value="3">метры</option>
                                                                <option value="4">штуки</option>
                                                                <option value="5">нарды</option>
                                                            </select>
                                                        </div>
                                                        <div class="image">
                                                            <a href="" class="fancybox img"></a>
                                                            <a href="">Загрузить</a>
                                                        </div>
                                                        <div class="advanced">
                                                            <input type="checkbox" id="advanced21"><label for="advanced21"></label>
                                                            <input type="checkbox" id="advanced22" checked><label for="advanced22"></label>
                                                            <input type="checkbox" id="advanced23"><label for="advanced23"></label>
                                                            <input type="checkbox" id="advanced24"><label for="advanced24"></label>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="title">
                                                            <span class="icon"></span>
                                                            <input id="service4" type="checkbox"><label for="service4">Ремонт замков-молний<label>
                                                        </div>
                                                        <div class="price error">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="price accurate error">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="unit">
                                                            <select name="" id="">
                                                                <option value="">- Выберите один из вариантов -</option>
                                                                <option value="0">гривны</option>
                                                                <option value="1">кучки</option>
                                                                <option value="2">грошики</option>
                                                                <option value="3">метры</option>
                                                                <option value="4">штуки</option>
                                                                <option value="5">нарды</option>
                                                            </select>
                                                        </div>
                                                        <div class="image">
                                                            <a href="" class="fancybox img"></a>
                                                            <a href="">Сменить</a>
                                                        </div>
                                                        <div class="advanced">
                                                            <input type="checkbox" id="advanced31" checked><label for="advanced31"></label>
                                                            <input type="checkbox" id="advanced32" checked><label for="advanced32"></label>
                                                            <input type="checkbox" id="advanced33" checked><label for="advanced33"></label>
                                                            <input type="checkbox" id="advanced34" checked><label for="advanced34"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="price" href="">Подгрузка собственного прайса в данной сфере</a>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="category">
                                            <div class="info">
                                                <div class="icon"></div>
                                                <span class="title">Строительство и ремонт</span>
                                            </div>
                                            <a href="" class="addphoto">Добавить фотографии</a>
                                            <div class="services">
                                                <header>
                                                    <div class="title">Название услуги:</div>
                                                    <div class="price">Стоимость от:</div>
                                                    <div class="price accurate">Точная стоимость:</div>
                                                    <div class="unit">Единицы изм.:</div>
                                                    <div class="image">Фотография:</div>
                                                    <div class="advanced">Доп. опции:</div>
                                                </header>
                                                <div class="items">
                                                    <div class="item">
                                                        <div class="title">
                                                            <span class="icon"></span>
                                                            <input id="service11" type="checkbox"><label for="service11">Шпатлевание</label>
                                                        </div>
                                                        <div class="price">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="price accurate">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="unit">
                                                            <select name="" id="">
                                                                <option value="">- Выберите один из вариантов -</option>
                                                                <option value="0">гривны</option>
                                                                <option value="1">кучки</option>
                                                                <option value="2">грошики</option>
                                                                <option value="3">метры</option>
                                                                <option value="4">штуки</option>
                                                                <option value="5">нарды</option>
                                                            </select>
                                                        </div>
                                                        <div class="image">
                                                            <a href="" class="fancybox img"></a>
                                                            <a href="">Сменить</a>
                                                        </div>
                                                        <div class="advanced">
                                                            <input type="checkbox" id="advanced41"><label for="advanced41"></label>
                                                            <input type="checkbox" id="advanced42"><label for="advanced42"></label>
                                                            <input type="checkbox" id="advanced43"><label for="advanced43"></label>
                                                            <input type="checkbox" id="advanced44" checked><label for="advanced44"></label>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="title">
                                                            <span class="icon"></span>
                                                            <input id="service12" type="checkbox"><label for="service12">Снятие размеров</label>
                                                        </div>
                                                        <div class="price">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="price accurate">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="unit">
                                                            <select name="" id="">
                                                                <option value="">- Выберите один из вариантов -</option>
                                                                <option value="0">гривны</option>
                                                                <option value="1">кучки</option>
                                                                <option value="2">грошики</option>
                                                                <option value="3">метры</option>
                                                                <option value="4">штуки</option>
                                                                <option value="5">нарды</option>
                                                            </select>
                                                        </div>
                                                        <div class="image">
                                                            <a href="" class="fancybox img"></a>
                                                            <a href="">Сменить</a>
                                                        </div>
                                                        <div class="advanced">
                                                            <input type="checkbox" id="advanced51"><label for="advanced51"></label>
                                                            <input type="checkbox" id="advanced52" checked><label for="advanced52"></label>
                                                            <input type="checkbox" id="advanced53"><label for="advanced53"></label>
                                                            <input type="checkbox" id="advanced54" checked><label for="advanced54"></label>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="title">
                                                            <span class="icon"></span>
                                                            <input id="service13" type="checkbox"><label for="service13">Бетонные работы</label>
                                                        </div>
                                                        <div class="price">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="price accurate">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="unit">
                                                            <select name="" id="">
                                                                <option value="">- Выберите один из вариантов -</option>
                                                                <option value="0">гривны</option>
                                                                <option value="1">кучки</option>
                                                                <option value="2">грошики</option>
                                                                <option value="3">метры</option>
                                                                <option value="4">штуки</option>
                                                                <option value="5">нарды</option>
                                                            </select>
                                                        </div>
                                                        <div class="image">
                                                            <a href="" class="fancybox img"></a>
                                                            <a href="">Сменить</a>
                                                        </div>
                                                        <div class="advanced">
                                                            <input type="checkbox" id="advanced61" checked><label for="advanced61"></label>
                                                            <input type="checkbox" id="advanced62"><label for="advanced62"></label>
                                                            <input type="checkbox" id="advanced63" checked><label for="advanced63"></label>
                                                            <input type="checkbox" id="advanced64"><label for="advanced64"></label>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="title">
                                                            <span class="icon"></span>
                                                            <input id="service14" type="checkbox"><label for="service14">Установка сантехники проч...<label>
                                                        </div>
                                                        <div class="price">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="price accurate">
                                                            <input type="text">руб.
                                                        </div>
                                                        <div class="unit">
                                                            <select name="" id="">
                                                                <option value="">- Выберите один из вариантов -</option>
                                                                <option value="0">гривны</option>
                                                                <option value="1">кучки</option>
                                                                <option value="2">грошики</option>
                                                                <option value="3">метры</option>
                                                                <option value="4">штуки</option>
                                                                <option value="5">нарды</option>
                                                            </select>
                                                        </div>
                                                        <div class="image">
                                                            <a href="" class="fancybox img"></a>
                                                            <a href="">Сменить</a>
                                                        </div>
                                                        <div class="advanced">
                                                            <input type="checkbox" id="advanced71" checked><label for="advanced71"></label>
                                                            <input type="checkbox" id="advanced72" checked><label for="advanced72"></label>
                                                            <input type="checkbox" id="advanced73"><label for="advanced73"></label>
                                                            <input type="checkbox" id="advanced74" checked><label for="advanced74"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="price" href="">Подгрузка собственного прайса в данной сфере</a>
                                            <div class="clear"></div>
                                        </div>
                                        <hr>
                                    </div>
                                </form>
                            </section>
                            <section class="tab" data-tab="advanced">
                                <form action="">
                                    <div class="row">
                                        <div class="icon"></div>
                                        <input id="checkbox1" type="checkbox">
                                        <label for="checkbox1">Дополнительная возможность</label>
                                    </div>
                                    <div class="row">
                                        <div class="icon"></div>
                                        <input id="checkbox2" type="checkbox">
                                        <label for="checkbox2">Дополнительная возможность</label>
                                    </div>
                                    <div class="row">
                                        <div class="icon"></div>
                                        <input id="checkbox3" type="checkbox" disabled>
                                        <label for="checkbox3">Дополнительная возможность</label>
                                    </div>
                                    <div class="row">
                                        <div class="icon"></div>
                                        <input id="checkbox4" type="checkbox">
                                        <label for="checkbox4">Дополнительная возможность</label>
                                    </div>
                                    <div class="row">
                                        <div class="icon"></div>
                                        <input id="checkbox5" type="checkbox">
                                        <label for="checkbox5">Дополнительная возможность</label>
                                    </div>

                                    <div class="row buttons">
                                        <input type="button" value="Отменить">
                                        <input type="submit" value="Сохранить">
                                    </div>
                                </form>
                            </section>
                            <section class="tab" data-tab="vip"></section>
                            <section class="tab" data-tab="packets">
                                <header>
                                    <span class="curent">Вы используете пакет "<span class="title marked">Оптимум</span>"</span>
                                    <div class="packets">
                                        <div class="item muted">
                                            <span class="title">Бесплатно:</span>
                                        </div>
                                        <div class="item">
                                            <span class="title">Стандарт:</span>
                                            <span class="price">9.90</span>
                                        </div>
                                        <div class="item marked">
                                            <span class="title">Оптимум:</span>
                                            <span class="price">39.90</span>
                                        </div>
                                        <div class="item">
                                            <span class="title">VIP:</span>
                                            <span class="price">99.90</span>
                                        </div>
                                    </div>
                                </header>
                                <div class="items">
                                    <div class="item">
                                        <span class="title">Выбор готовых шаблонов аккаунта </span>
                                        <div class="packets">
                                            <span>Да</span>
                                            <span>Нет</span>
                                            <span class="marked">Да</span>
                                            <span>Да</span>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <span class="title">Выбор готовых шаблонов аккаунта </span>
                                        <div class="packets">
                                            <span>Да</span>
                                            <span>Нет</span>
                                            <span class="marked">Да</span>
                                            <span>Нет</span>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <span class="title">Выбор готовых шаблонов аккаунта </span>
                                        <div class="packets">
                                            <span>Нет</span>
                                            <span>Да</span>
                                            <span class="marked">Да</span>
                                            <span>Да</span>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <span class="title">Выбор готовых шаблонов аккаунта </span>
                                        <div class="packets">
                                            <span>Да</span>
                                            <span>Да</span>
                                            <span class="marked">Да</span>
                                            <span>Да</span>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>