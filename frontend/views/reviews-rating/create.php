<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ReviewsRating $model
 */

$this->title = 'Добавить ответ';
$this->params['breadcrumbs'][] = ['label' => 'Отзывы: шаблоны ответов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reviews-rating-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
