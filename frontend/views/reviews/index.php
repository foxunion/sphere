<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\ReviewsSearch $searchModel
 */

$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;

?>
<div class="reviews-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить отзыв', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Услуга',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->idService->title;
                },
            ],
            [
                'attribute' => 'Пользователь',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->idUser->uid;
                },
            ],
            'text:ntext',
            'is_visible:boolean',
            'created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
