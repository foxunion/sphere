<div class="row address">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Почтовый адрес:</label>
        </div>
        <div class="form-group field-signupform-address-<?=$index?>-id_region">
            <select id="id_region<?=$index?>" class="chosen depdrop" name="SignupForm[address][<?=$index?>][id_region]" style="width: 130px;">
            <option value="">Регион</option>
            <option value="4">Красноярский край</option>
            </select><div class="help-block"></div>
        </div>       
        <div class="form-group field-signupform-address-<?=$index?>-id_city">
            <select id="id_city<?=$index?>" class="chosen depdrop" name="SignupForm[address][<?=$index?>][id_city]" placeholder="Город" style="width: 130px;" data-plugin-name="depdrop" data-plugin-options="depdrop_3b54ebbf" disabled="disabled" depends="['id_region<?=$index?>']" url="/site/city">
                <option value="">Город</option>
            </select>
            <div class="help-block"></div>
        </div>
        <div class="form-group field-signupform-address-<?=$index?>-id_area">
            <select id="id_area<?=$index?>" class="chosen depdrop" name="SignupForm[address][<?=$index?>][id_area]" placeholder="Исторический район" style="width: 130px;" data-plugin-name="depdrop" data-plugin-options="depdrop_e1e39e90" disabled="disabled" depends="['id_region<?=$index?>']" url="/site/city">
            <option value="">Исторический район</option>
            </select><div class="help-block"></div>
        </div>
        <div class="form-group field-signupform-address-<?=$index?>-id_street">
            <select id="id_street<?=$index?>" class="chosen depdrop" name="SignupForm[address][<?=$index?>][id_street]" placeholder="Улица" style="width: 130px;" data-plugin-name="depdrop" data-plugin-options="depdrop_f689d298" disabled="disabled" depends="['id_region<?=$index?>']" url="/site/city">
                <option value="">Улица</option>
            </select>
            <div class="help-block"></div>
        </div>
        <div class="form-group field-signupform-address-<?=$index?>-home">
            <input type="text" id="signupform-address-<?=$index?>-home" class="form-control" name="SignupForm[address][<?=$index?>][home]" placeholder="Дом" style="width: 80px;"><div class="help-block"></div>
        </div>        
        <div class="form-group field-signupform-address-<?=$index?>-apartment">
            <input type="text" id="signupform-address-<?=$index?>-apartment" class="form-control" name="SignupForm[address][<?=$index?>][apartment]" placeholder="Офис" style="width: 80px;"><div class="help-block"></div>
        </div>        
        <button class="add">+</button>
</div>