<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
/*
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\ContactForm */
/*
$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
*/
?>
<div class="site-contact">
    <p>
        Напишите нам, если у вас есть какие-либо пожелания или предложения. Спасибо.
    </p>
    <div>
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <div class="row">
                    <?= $form->field($model, 'body')->textArea(['rows' => 6, 'cols'=> 140]) ?>
                </div>
                 <div class="row inline">
                    <?= $form->field($model, 'subject', ['template'=>'{input}{error}'])->textInput(['placeholder' => $model->getAttributeLabel( 'subject' )]) ?>
                    <?= $form->field($model, 'name', ['template'=>'{input}{error}'])->textInput(['placeholder' => $model->getAttributeLabel( 'name' )]) ?>
                    <?= $form->field($model, 'email', ['template'=>'{input}{error}'])->textInput(['placeholder' => $model->getAttributeLabel( 'email' )]) ?>

                </div>
                <div class="row">
                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '{image}{input}',
                    ]) ?>
                </div>
                <div class="buttons">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
    </div>

</div>
