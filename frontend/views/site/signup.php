<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
</div>
<div class="personal">
    <h1>Регистрация</h1>
    <div class="tabs">
        <menu class="menu">
            <li class="disabled">
                <a href="" rel="messages"><span>Сообщения</span></a>
            </li>
            <li class="active">
                <a href="" rel="information"><span>Общая информация</span></a>
            </li>
            <li class="disabled">
                <a href="" rel="services"><span>Мои услуги</span></a>
            </li>
            <li class="disabled">
                <a href="" rel="advanced"><span>Дополнительные возможности</span></a>
            </li>
            <li class="disabled">
                <a href="" rel="vip"><span>Vip-услуги</span></a>
            </li>
            <li class="disabled">
                <a href="" rel="packets"><span>Пакеты опций</span></a>
            </li>
        </menu>

        <div class="sections">
            <section class="tab" data-tab="information">
                <?= $this->render('signupForm', [
                    'model' => $model,
                    'typeOrganization' =>  $typeOrganization,
                    'codesPhones' =>  $codesPhones,
                    'usersStatus' =>  $usersStatus,
                    'addressRegion' =>  $addressRegion,
                    'addressCity' =>  $addressCity,
                    'addressArea' =>  $addressArea,
                    'addressStreet' =>  $addressStreet,
                ]); ?>
            </section>
        </div>
    </div>
</div>