<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Вход в личный кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Вы можете зарегистрироваться в системе для получения пароля</p>
    <?= Html::a('Регистрация', ['site/signup'], ['class' => 'btn btn-primary']) ?>
    <p>Или войдите в систему, введя свой email и пароль.</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <div class="clearfix"></div>
                <?= $form->field($model, 'rememberMe', ['template'=>'{input} {label} {error}'])->checkbox([],false) ?>
                <div class="clearfix"></div>
                <div class="form-group">
                    <?= Html::submitButton('Вход', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
                <div class="clearfix"></div>
                <div style="color:#999;margin:1em 0">
                    Если вы забыли пароль, вы можете <?= Html::a('Восстановить его', ['site/request-password-reset']) ?>.
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
