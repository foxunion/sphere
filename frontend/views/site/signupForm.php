<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
use kartik\widgets\DepDrop;


?>

<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <input type="hidden" value="<?=Yii::$app->request->getCsrfToken()?>" />
    <div class="row">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Название фирмы/Имя частного лица:</label> 
        </div>
         <?= $form->field($model, 'id_typeOrganization', ['template'=>'{input}{error}'])->dropDownList(
                $typeOrganization,         
                ['prompt'=>'- Выберите тип -']
            );
        ?>
        <?= $form->field($model, 'username', ['template'=>'{input}{error}'])->textInput(['placeholder' => $model->getAttributeLabel( 'username' )]) ?>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Контактный телефон:</label>
        </div>
        <?= $form->field($model, 'id_codePhone', ['template'=>'{input}{error}'])->dropDownList(
                $codesPhones         
            );
        ?>
        <?= $form->field($model, 'phone', ['template'=>'{input}{error}'])->textInput(['placeholder' => $model->getAttributeLabel( 'phone' )]) ?>
        <div class="clearfix"></div>

    </div>
    <div class="row">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Режим работы:</label>
        </div>
        <?= $form->field($model, 'schedule', ['template'=>'{input}{error}'])->textInput(['placeholder' => $model->getAttributeLabel( 'schedule' )]) ?>
    </div>
    <div class="row">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Email-адрес:</label>
        </div>
        <?= $form->field($model, 'email', ['template'=>'{input}{error}'])->textInput(['placeholder' => $model->getAttributeLabel( 'email' )]) ?>
        <div class="clearfix"></div>
    </div>
    <?php for($i = 0; $i<=10; $i++){
        echo $this->render('_add_address_fields', array(
                  'form' => $form,
                  'addressRegion' =>  $addressRegion,
                  'addressCity' =>  $addressCity,
                  'addressArea' =>  $addressArea,
                  'addressStreet' =>  $addressStreet,
                  'model' => $model,
                  'index' => $i,
         ));
    } ?>
    
    <div class="row">
        <div class="icon"></div>
        <?= $form->field($model, 'onmap', ['template'=>'{input} {label} {error}'])->checkbox([],false) ?>
        <div class="clearfix"></div>
    </div>
    <div class="row image">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Установка логотипа:</label>
        </div>
        <?= $form->field($model, '_image', ['template'=>'<label class="control-label inputfile" for="signupform-_image">Загрузить логотип{input}<label>{error}'])->fileInput(['class'=>"button"]) ?>
        <div class="clear"></div>
    </div>
    <div class="row">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Ссылка на страницу (группу) "ВКонтакте":</label>
        </div>
        <?= $form->field($model, 'url_vk', ['template'=>'<div class="field-block"><span class="label">http://vk.com/</span>{input}</div>'])->textInput(['placeholder' => $model->getAttributeLabel( 'url_vk' )]) ?>
        <div class="clearfix"></div>
    
    </div>
    <div class="row">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Ссылка на собственный сайт:</label>
        </div>
        <?= $form->field($model, 'url_site', ['template'=>'<div class="field-block"><span class="label">http://</span>{input}</div>'])->textInput(['placeholder' => $model->getAttributeLabel( 'url_site' )]) ?>

        <div class="clearfix"></div>
    
    </div>
    <div class="row">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Указание текущего статуса:</label>
        </div>
         <?= $form->field($model, 'id_usersStatus', ['template'=>'{input}{error}'])->dropDownList(
            $usersStatus,         
            ['prompt'=>'- Выберите статус -']
        );
        ?>
        <div class="clearfix"></div>

    </div>
    <div class="row">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Придумайте пароль для входа на сайт:</label>
        </div>
        <?= $form->field($model, 'password', ['template'=>'{input}{error}'])->passwordInput() ?>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Повторите пароль:</label>
        </div>
        <?= $form->field($model, 'password_repeat', ['template'=>'{input}{error}'])->passwordInput() ?>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="icon"></div>
        <?= $form->field($model, 'captcha')->widget(Captcha::className()) ?>
        <div class="clearfix"></div>
    </div>

    <div class="row buttons">
        <?= Html::submitButton('Готово', ['class' => 'red', 'name' => 'signup-button']) ?>
        <div class="clearfix"></div>
    </div>
    

<?php ActiveForm::end(); ?>