<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\SignupForm */
?>
    <div class="row address <?php if(!isset($model->address[$index]) && $index!=0) echo 'hide';?>">
        <div class="icon"></div>
        <div class="form-group">
            <label for="">Почтовый адрес:</label>
        </div>
        <?= $form->field($model, 'address['.$index.'][id_region]', ['template'=>'{input}{error}'])->dropDownList(
                $addressRegion,         
                [
                    'prompt'=>'Регион', 
                    'style'=> "width: 130px;", 
                    'id'=>'id_region'.$index, 
                    'class'=>"chosen",
                ]
            );
        ?>
       
        <?= $form->field($model, 'address['.$index.'][id_city]', ['template'=>'{input}{error}'])->widget(DepDrop::classname(), [
            'data' => $addressCity,
            'options'=>[
                'id'=>'id_city'.$index, 
                'placeholder'=>'Город', 
                'class'=>"chosen",
                'style'=> "width: 130px;"
            ],
            'pluginOptions'=>[
                'depends'=>['id_region'.$index],
                'placeholder'=>'Город',
                'url'=>Url::to(['/site/city'])
            ]
        ]); ?>

        <?= $form->field($model, 'address['.$index.'][id_area]', ['template'=>'{input}{error}'])->widget(DepDrop::classname(), [
            'data' => $addressArea,
            'options'=>[
                'id'=>'id_area'.$index, 
                'placeholder'=>'Исторический район', 
                'style'=> "width: 130px;", 
                'class'=>"chosen"
            ],
            'pluginOptions'=>[
                'depends'=>['id_city'.$index],
                'placeholder'=>'Исторический район',
                'url'=>Url::to(['/site/area'])
            ]
        ]); ?>

        <?= $form->field($model, 'address['.$index.'][id_street]', ['template'=>'{input}{error}'])->widget(DepDrop::classname(), [
            'data' => $addressStreet,
            'options'=>[
                'id'=>'id_street'.$index, 
                'placeholder'=>'Улица', 
                'style'=> "width: 130px;", 
                'class'=>"chosen"
            ],
            'pluginOptions'=>[
                'depends'=>['id_area'.$index],
                'placeholder'=>'Улица',
                'url'=>Url::to(['/site/street'])
            ]
        ]); ?>

        <?= $form->field($model, 'address['.$index.'][house]', ['template'=>'{input}{error}'])->textInput(['placeholder' => "Дом", 'style'=>"width: 80px;"]) ?>
        <?= $form->field($model, 'address['.$index.'][apartment]', ['template'=>'{input}{error}'])->textInput(['placeholder' => "Офис", 'style'=>"width: 80px;"]) ?>
        <button class="add">+</button>
    </div>
                   