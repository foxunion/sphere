<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\ServicesCategorySearch $searchModel
 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;

?>
<h1><?= Html::encode($this->title) ?></h1>
<?if($model->servicesCategories){?>
<h3>Подкатегории</h3>
<div class="categories">
    <div class="items">
        <?php
        foreach ($model->servicesCategories as $key => $data)
            echo $this->render('_view',array(
                'data'=>$data,
            ));
        ?>
    </div>
</div>
<?}?>
<?php if($model->services){ ?> 
<h3>Услуги в этой сфере</h3>
<div class="categories">
    <div class="items">
        <?php
        foreach ($model->services as $key => $data)
            echo $this->render('/services/_view',array(
                'data'=>$data,
            ));
        ?>
    </div>
</div>
<?php } 
if(!$model->servicesCategories && !$model->services) {?>
    <p>В этой сфере нет предложений</p>
<?php } ?>
