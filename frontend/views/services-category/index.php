<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\ServicesCategorySearch $searchModel
 */

$this->title = 'Сферы';
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;

?>
<div class="categories">
    <div class="items">

        <?php
        foreach ($model as $key => $data)
            echo $this->render('_view',array(
                'data'=>$data,
                'key'=>@$key,
            ));
        ?>

    </div>
    <div class="clearfix"></div>
    <div class="buttons">
        <button id="showAll">Показать все <span class="number"><?=count($model)?></span> категорий</button>
        <button id='btnContact' class="red">Оставить заявку</button>
    </div>
    <div id="contactUs">
    </div>
</div>
