<div class="item <?php if(@$key>17) echo "hided";?>">
    <a href="/services-category/<?= $data->id;?>">
        <span class="icon" style="background-image: url( <?=$data->getImage()->getUrl('120x120') ?>);"></span>
        <span class="title"><?= $data->title; ?></span>
    </a>
    <?php if($data->servicesCategories){?>
        <div class="child <?php if((@$key+1)%5==0 || (@$key+1)%6==0) echo "right";?>">
            <a href="/services-category/<?= $data->id;?>">
                <span class="icon" style="background-image: url( <?=$data->getImage()->getUrl('120x120') ?>);"></span>
                <span class="title"><?= $data->title; ?></span>
            </a>
            <ul>
                <?php foreach ($data->servicesCategories as $key => $service) { 
                    if($key == 10)
                        break; 
                    ?>
                    <li>
                        <a href="/services-category/<?=$service->id;?>"><?= $service->title; ?></a>
                    </li>
                <?php }?>
                <li><a href="/services-category/<?= $data->id;?>">Посмотреть все подкатегории</a></li>
            </ul>
        </div>
    <?php }?>
</div>