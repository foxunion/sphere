<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Services $model
 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' =>$model->category->title , 'url' => ['/services-category/'.$model->category->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="services-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="left">
      <img src="<?=$model->getImage()->getUrl('200x200') ?>" alt="<?= Html::encode($this->title) ?>">
    </div>
    <div class="right">
      <?=$model->description?>
    </div>
    <div class="clearfix"></div>
</div>
<?php if($model->servicesDatas) {?>
<div class="search">
    <h3>Предложения для данной услуги</h3>
    <div class="items">
      <?php
      foreach ($model->servicesDatas as $key => $data)
          echo $this->render('/services-data/_view',array(
              'data'=>$data,
          ));
      ?>
    </div>
</div>
<?php } else { ?>
  <p>Нет предложений для данной услуги</p>
<?php }?>