<div class="item">
    <a href="/services/<?= $data->id;?>">
        <span class="icon" style="background-image: url( <?=$data->getImage()->getUrl('120x120') ?>);"></span>
        <span class="title"><?= $data->title; ?></span>
    </a>
</div>