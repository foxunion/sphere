<?php
namespace frontend\controllers;

use Yii;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

use common\models\TypeOrganization;
use common\models\CodesPhones;
use common\models\UsersStatus;

use common\models\UsersAddress;
use common\models\UsersAddressRegion;
use common\models\UsersAddressCity;
use common\models\UsersAddressArea;
use common\models\UsersAddressStreet;

use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'city'],
                'rules' => [
                    [
                        'actions' => ['signup', 'city'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->session->setFlash('flashMessage', array('success', 'Вы успешно вошли.'));
            return $this->redirect(['users-profile']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                return "Спасибо что свяались с нами. Ваше мнение очень важно для нас.";
            } else {
                return 'Произошла ошибка при отправке письма.';
            }
        } else {
            return $this->renderPartial('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    Yii::$app->session->setFlash('flashMessage', array('success', 'Вы успешно зарегистировались.'));
                    return $this->goHome();
                }
            }
        }

        $codesPhones = ArrayHelper::map(CodesPhones::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $typeOrganization = ArrayHelper::map(TypeOrganization::find()->select('id, value')->where('is_visible=1')->all(), 'id', 'value');
        $usersStatus = ArrayHelper::map(UsersStatus::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        
        $addressRegion = ArrayHelper::map(UsersAddressRegion::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $addressCity = ArrayHelper::map(UsersAddressCity::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $addressArea = ArrayHelper::map(UsersAddressArea::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $addressStreet = ArrayHelper::map(UsersAddressStreet::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        return $this->render('signup', [
            'model' => $model,
            'typeOrganization' =>  $typeOrganization,
            'codesPhones' =>  $codesPhones,
            'usersStatus' =>  $usersStatus,
            'addressRegion' =>  $addressRegion,
            'addressCity' =>  $addressCity,
            'addressArea' =>  $addressArea,
            'addressStreet' => $addressStreet
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    // add new address form
    public function actionField($index = false)
    {
        if($index == false){
            echo "";
            return;
        }
        $AddressRegion = ArrayHelper::map(UsersAddressRegion::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $model = new SignupForm();
        echo $this->renderPartial('_add_address_fields_ajax', array(
            'AddressRegion' =>  $AddressRegion,
            'model' => $model,
            'index' => $index,
        ));
    }


    // for dependet dropdown
    public function actionCity() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = ArrayHelper::map(UsersAddressCity::find()->select('id, title')->where(['is_visible' => 1 ,'id_region'=>$parents])->all(), 'id', 'title');
                echo Json::encode(['output' => $this->getDepDropData($out), 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'1', 'selected'=>'1']);
    }
     
    public function actionArea() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = ArrayHelper::map(UsersAddressArea::find()->select('id, title')->where(['is_visible' => 1 ,'id_city'=>$parents])->all(), 'id', 'title');
                echo Json::encode(['output' => $this->getDepDropData($out), 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionStreet() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = ArrayHelper::map(UsersAddressStreet::find()->select('id, title')->where(['is_visible' => 1 ,'id_area'=>$parents])->all(), 'id', 'title'); 
                echo Json::encode(['output' => $this->getDepDropData($out), 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    private function getDepDropData($in){ // форматирует данные для DepDrop
        $out = array( );
        foreach ($in as $key => $value) {
            $out[] = ['id'=>$key, "name"=> $value];
        }
        return $out;
    }
}
