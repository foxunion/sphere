<?php

namespace frontend\controllers;

use Yii;
use common\models\Users;
use common\models\UsersProfile;
use common\models\UsersStatus;
use common\models\search\UsersProfileSearch;


use common\models\TypeOrganization;
use common\models\CodesPhones;
use common\models\UsersAddressRegion;
use common\models\UsersAddressCity;
use common\models\UsersAddressArea;
use common\models\UsersAddressStreet;

use frontend\models\SignupForm;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

/*
 * UsersProfileController implements the CRUD actions for UsersProfile model.
 */
class UsersProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsersProfile models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*
        +кучамоделей для форм
        профиль
        услуги\
        сообщения
        донат
        */

        $model = Users::findOne(Yii::$app->user->id);
        $profileform = SignupForm::getForm(Yii::$app->user->id);
        $profile = UsersProfile::findOne(['id_user'=>Yii::$app->user->id]);
        
        $usersStatus = ArrayHelper::map(UsersStatus::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $typeOrganization = ArrayHelper::map(TypeOrganization::find()->select('id, value')->where('is_visible=1')->all(), 'id', 'value');
        $codesPhones = ArrayHelper::map(CodesPhones::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        
        $addressRegion = ArrayHelper::map(UsersAddressRegion::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $addressCity = ArrayHelper::map(UsersAddressCity::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $addressArea = ArrayHelper::map(UsersAddressArea::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        $addressStreet = ArrayHelper::map(UsersAddressStreet::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        return $this->render('index', [
            'model' => $model, 
            'profile' => $profile, 
            'profileform' => $profileform, 
            'usersStatus' => $usersStatus,
            'codesPhones' => $codesPhones,
            'typeOrganization' => $typeOrganization,
            'addressRegion' => $addressRegion, 
            'addressCity' =>  $addressCity,
            'addressArea' =>  $addressArea,
            'addressStreet' => $addressStreet
        ]);
    }

    /**
     * Displays a single UsersProfile model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UsersProfile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   
        $model = new UsersProfile;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
             
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UsersProfile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $usersStatus = ArrayHelper::map(UsersStatus::find()->select('id, title')->where('is_visible=1')->all(), 'id', 'title');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно обновлена.'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
             
            return $this->render('update', [
                'model' => $model,
                'usersStatus' => $usersStatus,
            ]);
        }
    }

    /**
     * Deletes an existing UsersProfile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('success', 'Запись успешно удалена.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the UsersProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsersProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsersProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
