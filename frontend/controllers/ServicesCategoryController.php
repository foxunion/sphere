<?php

namespace frontend\controllers;

use Yii;
use common\models\ServicesCategory;
use common\models\Services;
use common\models\search\ServicesCategorySearch;
use common\models\search\ServicesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ServicescategoryController implements the CRUD actions for ServicesCategory model.
 */
class ServicesCategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ServicesCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ServicesCategory;
        return $this->render('index', [
            'model' => $model->find()->where(array('parent_id'=>null, 'is_visible' => 1))->all()
        ]);
    }

    /**
     * Displays a single ServicesCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //$searchModel = new ServicesSearch;
        //$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            //'searchModel' => $searchModel,
            //'dataProvider' => $dataProvider,

        ]);
    }

    /**
     * Finds the ServicesCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ServicesCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServicesCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    
}
