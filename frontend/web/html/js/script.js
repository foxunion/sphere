var site = {
	init: function(){
		site.tabs();
		site.ui();
		site.mytime();
		site.messages(); // TODO: Удалить.

		$('select').chosen({'width':'auto'});
	},

	load: function(){
		$(window).load(function(){
			
		});
	},

	//Вкладки
	tabs: function(){
		var tabs = $('.tabs');
		var menu = tabs.children('.menu');
		tabs.find('.sections .tab[data-tab='+menu.find('li.active a').eq(0).attr('rel')+']').addClass('active');
		menu.find('li a').on('click', function(){
			if (!$(this).parent().hasClass('disabled'))
			{
				$(this).parent().siblings('li.active').removeClass('active');
				$(this).parent().addClass('active');
				tabs.find('.sections .tab.active').removeClass('active');
				tabs.find('.sections .tab[data-tab='+$(this).attr('rel')+']').addClass('active');
			}
			return false;
		});
	},

	// TODO: Удалить.
	messages: function(){
		var block = $('section[data-tab=messages]');
		var message = block.find('.right .message');
		var messages = block.find('.right .items li');
		var send = block.find('.right .send');
		send.hide();
		message.hide();
		messages.show();
		messages.find('a').click(function(event) {
			send.hide();
			messages.hide();
			message.show();
			return false;
		});
		block.find('.left button.send').click(function(){
			send.show();
			messages.hide();
			message.hide();
			return false;
		});
	},

	mytime: function(){
		var dates = {
			days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
			daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Вск"],
			daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
			months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
			monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
			today: "Сегодня",
			weekStart: 1
		};
		$("#service_date").pickmeup({
		    format  : 'Y-m-d',
		    hide_on_select: true,
		    locale: dates
		});
	},

	bpopup: function(){

	},

	ui: function(){
		//Двойной слайдер
		$('#slider-range').slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 75, 300 ],
		slide: function( event, ui ) {
			$( "#amount-range" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			$(this).find(".value-min").html("от " + ui.values[ 0 ] + " руб.");
			$(this).find(".value-max").html("до " + ui.values[ 1 ] + " руб.");
			}
		});
		$( "#amount-range" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			" - $" + $( "#slider-range" ).slider( "values", 1 ) );

	
		//Слайдер
		$( "#slider-range-min" ).slider({
			range: "min",
			value: 37,
			min: 1,
			max: 700,
			slide: function( event, ui ) {
				$( "#amount-range-min" ).val( "$" + ui.value );
				$(this).find(".value").html(ui.value + " руб.");
			}
		});
		$( "#amount-range-min" ).val( "$" + $( "#slider-range-min" ).slider( "value" ) );

		//Двойно слайдер со временем
		$('#time-range').slider({
			range: true,
			min: 0,
			max: 1440,
			step: 10,
			values: [ 480, 1080 ],
			slide: function( event, ui ) {
				$( "#time-slide-range" ).val( "от " + parseInt(ui.values[ 0 ]/60) + ":" + ui.values[ 0 ]%60  + " - " + "до " + parseInt(ui.values[ 1 ]/60) + ":" + ui.values[ 1 ]%60 );
				$(this).find(".value-min").html("от " + parseInt(ui.values[ 0 ]/60) + ":" + FormatMinute(ui.values[ 0 ]%60) );
				$(this).find(".value-max").html("до " + parseInt(ui.values[ 1 ]/60) + ":" + FormatMinute(ui.values[ 1 ]%60) );
				}
		});
		$( "#time-slide-range" ).val( "от " + parseInt($( "#time-range" ).slider( "values", 0 )/60) + ":" + $( "#time-range" ).slider( "values", 0 )%60  + " - " + "до " + parseInt($( "#time-range" ).slider( "values", 1 )/60) + ":" + $( "#time-range" ).slider( "values", 1 )%60 );
 		
		// добавляет "0" в начало минут
		function FormatMinute(val) {
			return (val<10 ? "0"+val : val);
		}

		/* Прогресс-бар */
		var progressbar = $( "#progressbar" ),
			progressLabel = $( ".progress-label" );
	 
		progressbar.progressbar({
			value: false,
			change: function() {
			progressLabel.text( progressbar.progressbar( "value" ) + "%" );
			},
			complete: function() {
			progressLabel.text( "Утюг!" );
			}
		});
	 
		function progress() {
			var val = progressbar.progressbar( "value" ) || 0;
	 
			progressbar.progressbar( "value", val + 1 );
	 
			if ( val < 99 ) {
			setTimeout( progress, 100 );
			}
		}
		setTimeout( progress, 3000 );
	},
}

$(document).ready(function(){
	site.init();
});