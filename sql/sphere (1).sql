-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 19 2014 г., 18:01
-- Версия сервера: 5.5.38-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `sphere`
--

-- --------------------------------------------------------

--
-- Структура таблицы `image`
--

CREATE TABLE IF NOT EXISTS `image` (
`id` int(11) NOT NULL,
  `filePath` varchar(400) NOT NULL,
  `itemId` int(20) NOT NULL,
  `isMain` int(1) DEFAULT NULL,
  `modelName` varchar(150) NOT NULL,
  `urlAlias` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1410770880),
('m130524_201442_init', 1410770915),
('m140622_111540_create_image_table', 1411119816);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_banners`
--

CREATE TABLE IF NOT EXISTS `tbl_banners` (
`id` int(11) NOT NULL,
  `title` varchar(128) DEFAULT NULL COMMENT 'Заголовок',
  `reference` varchar(255) DEFAULT NULL COMMENT 'Ссылка',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `place` varchar(45) DEFAULT NULL COMMENT 'Место',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tbl_banners`
--

INSERT INTO `tbl_banners` (`id`, `title`, `reference`, `image`, `place`, `is_visible`) VALUES
(3, '1', '2', '/var/www/advanced/frontend/../web/uploads/images/banners/3-889e390162fd34c94eed48b8ac50899e.jpg', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_codes_phones`
--

CREATE TABLE IF NOT EXISTS `tbl_codes_phones` (
`id` int(11) NOT NULL,
  `title` varchar(128) DEFAULT NULL COMMENT 'Заголовок',
  `value` varchar(128) DEFAULT NULL COMMENT 'Код',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_codes_phones`
--

INSERT INTO `tbl_codes_phones` (`id`, `title`, `value`, `is_visible`) VALUES
(1, '+7 391', '+7 391', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_pages`
--

CREATE TABLE IF NOT EXISTS `tbl_pages` (
`id` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL COMMENT 'Родитель',
  `path` varchar(128) DEFAULT NULL COMMENT 'Путь',
  `title` varchar(128) NOT NULL COMMENT 'Заголовок',
  `text` text COMMENT 'Текст',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_pages`
--

INSERT INTO `tbl_pages` (`id`, `id_parent`, `path`, `title`, `text`, `is_visible`) VALUES
(1, 1, 'glavnaya', 'Главная', '<p>Поздравляем вы зашли на страницу</p><p><img src="http://cs540105.vk.me/c620518/v620518242/1a6a4/hQ2z0V8qqsg.jpg"></p>', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reviews`
--

CREATE TABLE IF NOT EXISTS `tbl_reviews` (
`id` int(11) NOT NULL,
  `id_user` int(5) NOT NULL COMMENT 'Пользователь',
  `id_service` int(11) NOT NULL,
  `id_rating` int(11) NOT NULL,
  `text` text NOT NULL COMMENT 'Сообщение',
  `is_visible` tinyint(1) DEFAULT '0' COMMENT 'Видимость',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reviews_images`
--

CREATE TABLE IF NOT EXISTS `tbl_reviews_images` (
`id` int(11) NOT NULL,
  `id_review` int(11) NOT NULL COMMENT 'Отзыв',
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `description` text COMMENT 'Описание'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reviews_image_relations`
--

CREATE TABLE IF NOT EXISTS `tbl_reviews_image_relations` (
  `id_image` int(11) NOT NULL,
  `id_review` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reviews_rating`
--

CREATE TABLE IF NOT EXISTS `tbl_reviews_rating` (
`id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL COMMENT 'Заголовок',
  `rate` int(1) NOT NULL COMMENT 'Голос ',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_reviews_rating`
--

INSERT INTO `tbl_reviews_rating` (`id`, `title`, `rate`, `is_visible`) VALUES
(1, 'Все отлично!', 5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reviews_rating_relations`
--

CREATE TABLE IF NOT EXISTS `tbl_reviews_rating_relations` (
  `id_review` int(11) NOT NULL,
  `id_rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_services`
--

CREATE TABLE IF NOT EXISTS `tbl_services` (
`id` int(11) NOT NULL,
  `id_category` int(5) NOT NULL,
  `title` varchar(128) NOT NULL COMMENT 'Заго',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_services`
--

INSERT INTO `tbl_services` (`id`, `id_category`, `title`, `image`, `is_visible`) VALUES
(1, 1, 'услуга в тест', NULL, 1),
(2, 3, 'услуга в тест2', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_services_category`
--

CREATE TABLE IF NOT EXISTS `tbl_services_category` (
`id` int(11) NOT NULL,
  `parent_id` int(5) DEFAULT NULL,
  `title` varchar(128) NOT NULL COMMENT 'Заго',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `position` int(5) DEFAULT '0' COMMENT 'Позиция',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `tbl_services_category`
--

INSERT INTO `tbl_services_category` (`id`, `parent_id`, `title`, `image`, `position`, `is_visible`) VALUES
(1, NULL, 'Сфера1', NULL, NULL, 1),
(3, 1, 'Сфера2 от сферы1', NULL, NULL, 1),
(4, 1, 'Сфера3 от сферы1', NULL, NULL, 1),
(5, NULL, 'Сфера 4', NULL, NULL, 1),
(6, 4, 'Сфера 5 от сферы3', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_services_data`
--

CREATE TABLE IF NOT EXISTS `tbl_services_data` (
`id` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_unit` int(11) NOT NULL,
  `avg_price` varchar(45) DEFAULT NULL COMMENT 'Цена от',
  `price` varchar(45) DEFAULT NULL COMMENT 'Цена',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `worktime` timestamp NULL DEFAULT NULL COMMENT 'Время работы',
  `description` text COMMENT 'Описание',
  `discount` int(3) DEFAULT NULL COMMENT 'Скидка',
  `is_qualitymark` tinyint(1) DEFAULT '0' COMMENT 'Знак качества',
  `is_recomendated` tinyint(1) DEFAULT '0' COMMENT 'Рекомендуем'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_services_data`
--

INSERT INTO `tbl_services_data` (`id`, `id_service`, `id_user`, `id_unit`, `avg_price`, `price`, `image`, `worktime`, `description`, `discount`, `is_qualitymark`, `is_recomendated`) VALUES
(1, 2, 6, 1, '10', '100', 'images/services-data/1-4dc9592843a4fca36e64bfb70e8f407f.jpg', '2014-07-16 07:34:25', 'dawdqwdqdqdqfqfq', NULL, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_services_files`
--

CREATE TABLE IF NOT EXISTS `tbl_services_files` (
`id` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `file` varchar(255) NOT NULL COMMENT 'Файл'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_services_images`
--

CREATE TABLE IF NOT EXISTS `tbl_services_images` (
`id` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `description` varchar(200) DEFAULT NULL COMMENT 'Описание'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_services_similar`
--

CREATE TABLE IF NOT EXISTS `tbl_services_similar` (
`id` int(11) NOT NULL,
  `id_parent_service` int(11) DEFAULT NULL,
  `id_service` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_services_units`
--

CREATE TABLE IF NOT EXISTS `tbl_services_units` (
`id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL COMMENT 'Заголовок',
  `unit` varchar(45) NOT NULL COMMENT 'Величина',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_services_units`
--

INSERT INTO `tbl_services_units` (`id`, `title`, `unit`, `is_visible`) VALUES
(1, '12', '21', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_type_organization`
--

CREATE TABLE IF NOT EXISTS `tbl_type_organization` (
`id` int(11) NOT NULL,
  `title` varchar(128) DEFAULT NULL COMMENT 'Полное название типа организации',
  `value` varchar(128) DEFAULT NULL COMMENT 'Аббревиатура типа организации',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
`id` int(11) NOT NULL,
  `uid` varchar(45) DEFAULT NULL,
  `email` varchar(255) NOT NULL COMMENT 'Электронная почта',
  `password` varchar(255) NOT NULL COMMENT 'Пароль',
  `salt` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `uid`, `email`, `password`, `salt`, `created`, `updated`) VALUES
(1, 'admin', 'admin', '', NULL, '2014-07-28 06:27:09', NULL),
(5, 'test2', 'test2@test2.ru', 'a390cdcc24b338ac1b8761bab42f862d', 'mj6wqjhmu6a6q34pxmwqqxxhpyfpv2y5', '2014-07-23 05:21:13', NULL),
(6, 'Новый пользователь', 'newuser@newuser.ru', '61d54223b9be0ae87ed6fee42b35d496', 'p8doitjwvemdyq7kcc4b2xgwke6jicax', '2014-07-23 08:18:56', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_address`
--

CREATE TABLE IF NOT EXISTS `tbl_users_address` (
`id` int(11) NOT NULL,
  `id_user` int(5) NOT NULL COMMENT 'Пользователь',
  `id_region` int(5) DEFAULT NULL COMMENT 'Рег',
  `id_city` int(5) DEFAULT NULL COMMENT 'Город',
  `id_street` int(10) DEFAULT NULL COMMENT 'Улица',
  `id_area` int(10) DEFAULT NULL,
  `house` varchar(10) DEFAULT NULL COMMENT 'Дом',
  `apartment` int(3) DEFAULT NULL COMMENT 'Квартира',
  `is_map` tinyint(1) DEFAULT '0' COMMENT 'Показывать на карте'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `tbl_users_address`
--

INSERT INTO `tbl_users_address` (`id`, `id_user`, `id_region`, `id_city`, `id_street`, `id_area`, `house`, `apartment`, `is_map`) VALUES
(13, 6, 4, 2, 1, 1, '11', 222, 1),
(14, 6, 4, 2, 1, 2, '', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_address_area`
--

CREATE TABLE IF NOT EXISTS `tbl_users_address_area` (
`id` int(11) NOT NULL,
  `id_city` int(11) NOT NULL COMMENT 'Город',
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `is_visible` tinyint(1) DEFAULT NULL COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_users_address_area`
--

INSERT INTO `tbl_users_address_area` (`id`, `id_city`, `title`, `is_visible`) VALUES
(1, 2, 'Взлетка', 1),
(2, 2, 'Северный', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_address_city`
--

CREATE TABLE IF NOT EXISTS `tbl_users_address_city` (
`id` int(11) NOT NULL,
  `id_region` int(10) NOT NULL COMMENT 'Регион',
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_users_address_city`
--

INSERT INTO `tbl_users_address_city` (`id`, `id_region`, `title`, `is_visible`) VALUES
(2, 4, 'Красноярск', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_address_region`
--

CREATE TABLE IF NOT EXISTS `tbl_users_address_region` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `is_visible` int(1) NOT NULL DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tbl_users_address_region`
--

INSERT INTO `tbl_users_address_region` (`id`, `title`, `is_visible`) VALUES
(4, 'Красноярский край', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_address_street`
--

CREATE TABLE IF NOT EXISTS `tbl_users_address_street` (
`id` int(11) NOT NULL,
  `id_area` int(5) NOT NULL COMMENT 'Город',
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Види'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_users_address_street`
--

INSERT INTO `tbl_users_address_street` (`id`, `id_area`, `title`, `is_visible`) VALUES
(1, 1, '9 Мая', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_donate`
--

CREATE TABLE IF NOT EXISTS `tbl_users_donate` (
`id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_groups`
--

CREATE TABLE IF NOT EXISTS `tbl_users_groups` (
`id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL COMMENT 'Заголовок',
  `price` int(20) NOT NULL COMMENT 'Цена',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Дамп данных таблицы `tbl_users_groups`
--

INSERT INTO `tbl_users_groups` (`id`, `title`, `price`, `is_visible`) VALUES
(29, 'test', 1231, 0),
(31, '1', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_groups_options`
--

CREATE TABLE IF NOT EXISTS `tbl_users_groups_options` (
`id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL COMMENT 'Заголовок',
  `desc` text COMMENT 'Описание',
  `is_visible` tinyint(1) DEFAULT NULL COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `tbl_users_groups_options`
--

INSERT INTO `tbl_users_groups_options` (`id`, `title`, `desc`, `is_visible`) VALUES
(2, 'Выбор готовых шаблонов аккаунта', 'Предлагаемый временный текст написан разработчиком с целью демонстрации заполнения блока текстовым содержимым. После окончания работы - этот текст будет удален и заменен постоянным. А сейчас для более полного заполнения блока текстовой инфор мацией этот текст будет повторен многократно.', 1),
(3, 'Название фирмы', 'Предлагаемый временный текст написан разработчиком с целью демонстрации заполнения блока текстовым содержимым. После окончания работы - этот текст будет удален и заменен постоянным. А сейчас для более полного заполнения блока текстовой инфор мацией этот текст будет повторен многократно.', 1),
(4, 'Email-адрес', 'Предлагаемый временный текст написан разработчиком с целью демонстрации заполнения блока текстовым содержимым. После окончания работы - этот текст будет удален и заменен постоянным. А сейчас для более полного заполнения блока текстовой инфор мацией этот текст будет повторен многократно.\r\n', 1),
(5, 'Указание стоимости своих услуг', 'Предлагаемый временный текст написан разработчиком с целью демонстрации заполнения блока текстовым содержимым. После окончания работы - этот текст будет удален и заменен постоянным. А сейчас для более полного заполнения блока текстовой инфор мацией этот текст будет повторен многократно.', 1),
(6, 'Участие в рейтингах', 'Предлагаемый временный текст написан разработчиком с целью демонстрации заполнения блока текстовым содержимым. После окончания работы - этот текст будет удален и заменен постоянным. А сейчас для более полного заполнения блока текстовой инфор мацией этот текст будет повторен многократно.', 1),
(7, 'Загрузка логотипа', 'Предлагаемый временный текст написан разработчиком с целью демонстрации заполнения блока текстовым содержимым. После окончания работы - этот текст будет удален и заменен постоянным. А сейчас для более полного заполнения блока текстовой инфор мацией этот текст будет повторен многократно.', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_groups_option_relations`
--

CREATE TABLE IF NOT EXISTS `tbl_users_groups_option_relations` (
  `id_packet` int(5) NOT NULL,
  `id_packet_option` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_users_groups_option_relations`
--

INSERT INTO `tbl_users_groups_option_relations` (`id_packet`, `id_packet_option`) VALUES
(29, 3),
(29, 6),
(29, 7),
(31, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_groups_relations`
--

CREATE TABLE IF NOT EXISTS `tbl_users_groups_relations` (
  `id_user` int(11) NOT NULL,
  `id_group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_groups_rules`
--

CREATE TABLE IF NOT EXISTS `tbl_users_groups_rules` (
`id` int(11) NOT NULL COMMENT 'ID',
  `id_group` int(11) NOT NULL COMMENT 'Группа',
  `table` varchar(45) NOT NULL COMMENT 'Таблица',
  `option` varchar(45) NOT NULL COMMENT 'Опции'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_message`
--

CREATE TABLE IF NOT EXISTS `tbl_users_message` (
`id` int(11) NOT NULL,
  `id_sender` int(11) NOT NULL COMMENT 'Отправитель',
  `id_recipient` int(11) NOT NULL COMMENT 'Получатель',
  `subject` varchar(255) DEFAULT NULL COMMENT 'Тема',
  `text` text COMMENT 'Сообщение',
  `type` int(1) DEFAULT NULL COMMENT 'Тип',
  `status` int(1) DEFAULT NULL COMMENT 'Статус',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_my_views`
--

CREATE TABLE IF NOT EXISTS `tbl_users_my_views` (
`id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `is_bookmark` tinyint(1) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_phones`
--

CREATE TABLE IF NOT EXISTS `tbl_users_phones` (
`id` int(11) NOT NULL,
  `id_user` int(5) DEFAULT NULL,
  `id_code` int(11) DEFAULT NULL COMMENT 'Код',
  `phone` varchar(45) DEFAULT NULL COMMENT 'Тел'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `tbl_users_phones`
--

INSERT INTO `tbl_users_phones` (`id`, `id_user`, `id_code`, `phone`) VALUES
(6, 5, 1, '32131'),
(7, 5, 1, '231'),
(8, 6, 1, '32131'),
(9, 6, 1, '323123');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_profile`
--

CREATE TABLE IF NOT EXISTS `tbl_users_profile` (
`id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_status` int(11) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL COMMENT 'Заголовок',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `balance` float DEFAULT NULL COMMENT 'Баланс',
  `bonus` int(11) DEFAULT NULL COMMENT 'Бо',
  `url_vk` varchar(45) DEFAULT NULL COMMENT 'Ссылка вконтакте',
  `url_www` varchar(45) DEFAULT NULL COMMENT 'Ссылка личный сайт',
  `schedule` timestamp NULL DEFAULT NULL COMMENT 'Время работы'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tbl_users_profile`
--

INSERT INTO `tbl_users_profile` (`id`, `id_user`, `id_status`, `title`, `image`, `balance`, `bonus`, `url_vk`, `url_www`, `schedule`) VALUES
(1, 5, 1, '1', 'images/profile/1-e8caf45499714c219d1888136ef73f9d.jpg', 1, 1, '', '', NULL),
(2, 6, NULL, '1', 'images/profile/2-13a03a68213ac4c061a397252b3c8bb4.jpg', NULL, NULL, '', '', NULL),
(3, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_rating`
--

CREATE TABLE IF NOT EXISTS `tbl_users_rating` (
`id` int(11) NOT NULL,
  `id_user` int(5) NOT NULL,
  `vote` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_status`
--

CREATE TABLE IF NOT EXISTS `tbl_users_status` (
`id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL COMMENT 'Заголовок',
  `is_visible` tinyint(1) DEFAULT '1' COMMENT 'Видимость'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_users_status`
--

INSERT INTO `tbl_users_status` (`id`, `title`, `is_visible`) VALUES
(1, 'test', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users_views`
--

CREATE TABLE IF NOT EXISTS `tbl_users_views` (
`id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'ehGsNGYJHS-lkQdpBM6B2k5yKQObzS9G', '$2y$13$5hlpZbIk6/SjglrsbFN6jeqpxRtaq8HGHUgY3K9IRUmqtr/Boy6hi', NULL, 'alfimovd@gmail.com', 10, 10, 1410854524, 1410854524);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `image`
--
ALTER TABLE `image`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
 ADD PRIMARY KEY (`version`);

--
-- Indexes for table `tbl_banners`
--
ALTER TABLE `tbl_banners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_codes_phones`
--
ALTER TABLE `tbl_codes_phones`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_pages`
--
ALTER TABLE `tbl_pages`
 ADD PRIMARY KEY (`id`), ADD KEY `to_parent_idx` (`id_parent`);

--
-- Indexes for table `tbl_reviews`
--
ALTER TABLE `tbl_reviews`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_tbl_reviews_1_idx` (`id_user`), ADD KEY `id_service` (`id_service`), ADD KEY `id_ratings` (`id_rating`);

--
-- Indexes for table `tbl_reviews_images`
--
ALTER TABLE `tbl_reviews_images`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reviews_image_relations`
--
ALTER TABLE `tbl_reviews_image_relations`
 ADD KEY `to_review_idx` (`id_review`), ADD KEY `to_image_idx` (`id_image`);

--
-- Indexes for table `tbl_reviews_rating`
--
ALTER TABLE `tbl_reviews_rating`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reviews_rating_relations`
--
ALTER TABLE `tbl_reviews_rating_relations`
 ADD KEY `to_rating_idx` (`id_rating`), ADD KEY `to_review_idx` (`id_review`);

--
-- Indexes for table `tbl_services`
--
ALTER TABLE `tbl_services`
 ADD PRIMARY KEY (`id`), ADD KEY `tbl_services_idx` (`id_category`);

--
-- Indexes for table `tbl_services_category`
--
ALTER TABLE `tbl_services_category`
 ADD PRIMARY KEY (`id`), ADD KEY `faga_idx` (`parent_id`), ADD KEY `index3` (`parent_id`);

--
-- Indexes for table `tbl_services_data`
--
ALTER TABLE `tbl_services_data`
 ADD PRIMARY KEY (`id`), ADD KEY `to_user_idx` (`id_user`), ADD KEY `to_services_idx` (`id_service`), ADD KEY `to_unit_idx` (`id_unit`);

--
-- Indexes for table `tbl_services_files`
--
ALTER TABLE `tbl_services_files`
 ADD PRIMARY KEY (`id`), ADD KEY `to_user_idx` (`id_user`), ADD KEY `to_service_idx` (`id_service`);

--
-- Indexes for table `tbl_services_images`
--
ALTER TABLE `tbl_services_images`
 ADD PRIMARY KEY (`id`), ADD KEY `to_service_category_idx` (`id_service`), ADD KEY `to_user_idx` (`id_user`);

--
-- Indexes for table `tbl_services_similar`
--
ALTER TABLE `tbl_services_similar`
 ADD PRIMARY KEY (`id`), ADD KEY `to_service_idx` (`id_parent_service`), ADD KEY `to_servie_idx` (`id_service`);

--
-- Indexes for table `tbl_services_units`
--
ALTER TABLE `tbl_services_units`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_type_organization`
--
ALTER TABLE `tbl_type_organization`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email_UNIQUE` (`email`), ADD UNIQUE KEY `uid_UNIQUE` (`uid`);

--
-- Indexes for table `tbl_users_address`
--
ALTER TABLE `tbl_users_address`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_tbl_address_1_idx` (`id_user`), ADD KEY `fk_tbl_address_2_idx` (`id_region`), ADD KEY `fk_tbl_address_3_idx` (`id_city`), ADD KEY `fk_tbl_address_4_idx` (`id_street`), ADD KEY `to_area_idx` (`id_area`);

--
-- Indexes for table `tbl_users_address_area`
--
ALTER TABLE `tbl_users_address_area`
 ADD PRIMARY KEY (`id`), ADD KEY `to_city_idx` (`id_city`);

--
-- Indexes for table `tbl_users_address_city`
--
ALTER TABLE `tbl_users_address_city`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_tbl_city_1_idx` (`id_region`);

--
-- Indexes for table `tbl_users_address_region`
--
ALTER TABLE `tbl_users_address_region`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users_address_street`
--
ALTER TABLE `tbl_users_address_street`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_tbl_area_1_idx` (`id_area`);

--
-- Indexes for table `tbl_users_donate`
--
ALTER TABLE `tbl_users_donate`
 ADD PRIMARY KEY (`id`), ADD KEY `to_user_profile_idx` (`id_user`);

--
-- Indexes for table `tbl_users_groups`
--
ALTER TABLE `tbl_users_groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users_groups_options`
--
ALTER TABLE `tbl_users_groups_options`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users_groups_option_relations`
--
ALTER TABLE `tbl_users_groups_option_relations`
 ADD KEY `fk_tbl_packet_relations_1_idx` (`id_packet`), ADD KEY `fk_tbl_packet_relations_2_idx` (`id_packet_option`);

--
-- Indexes for table `tbl_users_groups_relations`
--
ALTER TABLE `tbl_users_groups_relations`
 ADD KEY `to_user_idx` (`id_user`), ADD KEY `to_group_idx` (`id_group`);

--
-- Indexes for table `tbl_users_groups_rules`
--
ALTER TABLE `tbl_users_groups_rules`
 ADD PRIMARY KEY (`id`), ADD KEY `to_users_group_idx` (`id_group`);

--
-- Indexes for table `tbl_users_message`
--
ALTER TABLE `tbl_users_message`
 ADD PRIMARY KEY (`id`), ADD KEY `user_message_idx` (`id_sender`), ADD KEY `to_user_idx` (`id_recipient`);

--
-- Indexes for table `tbl_users_my_views`
--
ALTER TABLE `tbl_users_my_views`
 ADD PRIMARY KEY (`id`), ADD KEY `user_myviews_idx` (`id_user`), ADD KEY `to_services_data_idx` (`id_item`);

--
-- Indexes for table `tbl_users_phones`
--
ALTER TABLE `tbl_users_phones`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_tbl_phones_1_idx` (`id_user`), ADD KEY `id_code` (`id_code`);

--
-- Indexes for table `tbl_users_profile`
--
ALTER TABLE `tbl_users_profile`
 ADD PRIMARY KEY (`id`), ADD KEY `users_profile_idx` (`id_user`), ADD KEY `user_profile_status_fk` (`id_status`);

--
-- Indexes for table `tbl_users_rating`
--
ALTER TABLE `tbl_users_rating`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_tbl_rating_1_idx` (`id_user`);

--
-- Indexes for table `tbl_users_status`
--
ALTER TABLE `tbl_users_status`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_users_views`
--
ALTER TABLE `tbl_users_views`
 ADD PRIMARY KEY (`id`), ADD KEY `to_user_idx` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_banners`
--
ALTER TABLE `tbl_banners`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_codes_phones`
--
ALTER TABLE `tbl_codes_phones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_pages`
--
ALTER TABLE `tbl_pages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_reviews`
--
ALTER TABLE `tbl_reviews`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_reviews_images`
--
ALTER TABLE `tbl_reviews_images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_reviews_rating`
--
ALTER TABLE `tbl_reviews_rating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_services`
--
ALTER TABLE `tbl_services`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_services_category`
--
ALTER TABLE `tbl_services_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_services_data`
--
ALTER TABLE `tbl_services_data`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_services_files`
--
ALTER TABLE `tbl_services_files`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_services_images`
--
ALTER TABLE `tbl_services_images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_services_similar`
--
ALTER TABLE `tbl_services_similar`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_services_units`
--
ALTER TABLE `tbl_services_units`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_type_organization`
--
ALTER TABLE `tbl_type_organization`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_users_address`
--
ALTER TABLE `tbl_users_address`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_users_address_area`
--
ALTER TABLE `tbl_users_address_area`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_users_address_city`
--
ALTER TABLE `tbl_users_address_city`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_users_address_region`
--
ALTER TABLE `tbl_users_address_region`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_users_address_street`
--
ALTER TABLE `tbl_users_address_street`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_users_donate`
--
ALTER TABLE `tbl_users_donate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users_groups`
--
ALTER TABLE `tbl_users_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tbl_users_groups_options`
--
ALTER TABLE `tbl_users_groups_options`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_users_groups_rules`
--
ALTER TABLE `tbl_users_groups_rules`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `tbl_users_message`
--
ALTER TABLE `tbl_users_message`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users_my_views`
--
ALTER TABLE `tbl_users_my_views`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users_phones`
--
ALTER TABLE `tbl_users_phones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_users_profile`
--
ALTER TABLE `tbl_users_profile`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_users_rating`
--
ALTER TABLE `tbl_users_rating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users_status`
--
ALTER TABLE `tbl_users_status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_users_views`
--
ALTER TABLE `tbl_users_views`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tbl_pages`
--
ALTER TABLE `tbl_pages`
ADD CONSTRAINT `fk_pages_to_parent` FOREIGN KEY (`id_parent`) REFERENCES `tbl_pages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_reviews`
--
ALTER TABLE `tbl_reviews`
ADD CONSTRAINT `fk_tbl_reviews_1` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `tbl_reviews_ibfk_1` FOREIGN KEY (`id_rating`) REFERENCES `tbl_reviews_rating` (`id`),
ADD CONSTRAINT `tbl_reviews_ibfk_2` FOREIGN KEY (`id_service`) REFERENCES `tbl_services` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_reviews_image_relations`
--
ALTER TABLE `tbl_reviews_image_relations`
ADD CONSTRAINT `fk_reviews_to_image` FOREIGN KEY (`id_image`) REFERENCES `tbl_reviews_images` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_reviews_to_review2` FOREIGN KEY (`id_review`) REFERENCES `tbl_reviews` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_reviews_rating_relations`
--
ALTER TABLE `tbl_reviews_rating_relations`
ADD CONSTRAINT `fk_reviews_to_rating` FOREIGN KEY (`id_rating`) REFERENCES `tbl_reviews_rating` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_reviews_to_review` FOREIGN KEY (`id_review`) REFERENCES `tbl_reviews` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_services`
--
ALTER TABLE `tbl_services`
ADD CONSTRAINT `fk_services` FOREIGN KEY (`id_category`) REFERENCES `tbl_services_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_services_category`
--
ALTER TABLE `tbl_services_category`
ADD CONSTRAINT `fk_scategory_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `tbl_services_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_services_data`
--
ALTER TABLE `tbl_services_data`
ADD CONSTRAINT `fk_sservice_services` FOREIGN KEY (`id_service`) REFERENCES `tbl_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sservice_to_user` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sservice_unit` FOREIGN KEY (`id_unit`) REFERENCES `tbl_services_units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_services_images`
--
ALTER TABLE `tbl_services_images`
ADD CONSTRAINT `to_service_category` FOREIGN KEY (`id_service`) REFERENCES `tbl_services_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `to_user` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_services_similar`
--
ALTER TABLE `tbl_services_similar`
ADD CONSTRAINT `fk_sservice_service` FOREIGN KEY (`id_parent_service`) REFERENCES `tbl_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sservice_service2` FOREIGN KEY (`id_service`) REFERENCES `tbl_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_users_address`
--
ALTER TABLE `tbl_users_address`
ADD CONSTRAINT `fk_tbl_address_1` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_tbl_address_3` FOREIGN KEY (`id_city`) REFERENCES `tbl_users_address_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_tbl_address_4` FOREIGN KEY (`id_street`) REFERENCES `tbl_users_address_street` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_user_to_area` FOREIGN KEY (`id_area`) REFERENCES `tbl_users_address_area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `tbl_users_address_ibfk_1` FOREIGN KEY (`id_region`) REFERENCES `tbl_users_address_region` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_users_address_area`
--
ALTER TABLE `tbl_users_address_area`
ADD CONSTRAINT `fk_user_to_city` FOREIGN KEY (`id_city`) REFERENCES `tbl_users_address_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_users_address_city`
--
ALTER TABLE `tbl_users_address_city`
ADD CONSTRAINT `tbl_users_address_city_ibfk_1` FOREIGN KEY (`id_region`) REFERENCES `tbl_users_address_region` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_users_address_street`
--
ALTER TABLE `tbl_users_address_street`
ADD CONSTRAINT `fk_user_fk_tbl_area_1` FOREIGN KEY (`id_area`) REFERENCES `tbl_users_address_area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_users_donate`
--
ALTER TABLE `tbl_users_donate`
ADD CONSTRAINT `fk_user_to_user_profile` FOREIGN KEY (`id_user`) REFERENCES `tbl_users_profile` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_users_groups_option_relations`
--
ALTER TABLE `tbl_users_groups_option_relations`
ADD CONSTRAINT `fk_tbl_packet_relations_1` FOREIGN KEY (`id_packet`) REFERENCES `tbl_users_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tbl_packet_relations_2` FOREIGN KEY (`id_packet_option`) REFERENCES `tbl_users_groups_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_users_groups_relations`
--
ALTER TABLE `tbl_users_groups_relations`
ADD CONSTRAINT `fk_user_group2` FOREIGN KEY (`id_group`) REFERENCES `tbl_users_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_user_to_user3` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_users_groups_rules`
--
ALTER TABLE `tbl_users_groups_rules`
ADD CONSTRAINT `fk_user_to_users_group` FOREIGN KEY (`id_group`) REFERENCES `tbl_users_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_users_message`
--
ALTER TABLE `tbl_users_message`
ADD CONSTRAINT `fk_user_to_user` FOREIGN KEY (`id_recipient`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_user_user_message` FOREIGN KEY (`id_sender`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_users_my_views`
--
ALTER TABLE `tbl_users_my_views`
ADD CONSTRAINT `fk_user_to_services_data` FOREIGN KEY (`id_item`) REFERENCES `tbl_services_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `user_myviews` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_users_phones`
--
ALTER TABLE `tbl_users_phones`
ADD CONSTRAINT `tbl_users_phones_ibfk_1` FOREIGN KEY (`id_code`) REFERENCES `tbl_codes_phones` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_users_profile`
--
ALTER TABLE `tbl_users_profile`
ADD CONSTRAINT `fk_user_users_profile` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_users_profile_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `tbl_users_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_users_rating`
--
ALTER TABLE `tbl_users_rating`
ADD CONSTRAINT `fk_tbl_rating_1` FOREIGN KEY (`id_user`) REFERENCES `tbl_users_profile` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `tbl_users_views`
--
ALTER TABLE `tbl_users_views`
ADD CONSTRAINT `fk_user_user2` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
