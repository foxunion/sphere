<?php
return [
    'adminEmail' => 'alfimovd@gmail.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
