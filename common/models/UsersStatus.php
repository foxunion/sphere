<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_status".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_visible
 *
 * @property TblUsersProfile[] $tblUsersProfiles
 */
class UsersStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['is_visible'], 'integer'],
            [['title'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersProfiles()
    {
        return $this->hasMany(TblUsersProfile::className(), ['id_status' => 'id']);
    }
}
