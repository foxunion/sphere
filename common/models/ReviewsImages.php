<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_reviews_images".
 *
 * @property integer $id
 * @property integer $id_review
 * @property string $image
 * @property string $description
 *
 * @property TbLReviewsImagesHasTblReviews $tbLReviewsImagesHasTblReviews
 * @property TblReviews[] $tblReviews
 * @property TblReviewsImageRelations[] $tblReviewsImageRelations
 */
class ReviewsImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_reviews_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_review', 'image'], 'required'],
            [['id_review'], 'integer'],
            [['description'], 'string'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_review' => 'Отзыв',
            'image' => 'Изображение',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbLReviewsImagesHasTblReviews()
    {
        return $this->hasOne(TbLReviewsImagesHasTblReviews::className(), ['tbL_reviews_images_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblReviews()
    {
        return $this->hasMany(TblReviews::className(), ['id' => 'tbl_reviews_id'])->viaTable('tbL_reviews_images_has_tbl_reviews', ['tbL_reviews_images_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblReviewsImageRelations()
    {
        return $this->hasMany(TblReviewsImageRelations::className(), ['id_image' => 'id']);
    }
}
