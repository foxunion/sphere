<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_services_units".
 *
 * @property integer $id
 * @property string $title
 * @property string $unit
 * @property integer $is_visible
 *
 * @property TblServicesData[] $tblServicesDatas
 */
class ServicesUnits extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_services_units';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'unit'], 'required'],
            [['is_visible'], 'integer'],
            [['title'], 'string', 'max' => 128],
            [['unit'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'unit' => 'Величина',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblServicesDatas()
    {
        return $this->hasMany(TblServicesData::className(), ['id_unit' => 'id']);
    }
}
