<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_services_similar".
 *
 * @property integer $id
 * @property integer $id_parent_service
 * @property integer $id_service
 *
 * @property TblServices $idParentService
 * @property TblServices $idService
 */
class ServicesSimilar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_services_similar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent_service', 'id_service'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent_service' => 'Id Parent Service',
            'id_service' => 'Id Service',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdParentService()
    {
        return $this->hasOne(TblServices::className(), ['id' => 'id_parent_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdService()
    {
        return $this->hasOne(TblServices::className(), ['id' => 'id_service']);
    }
}
