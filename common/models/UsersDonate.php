<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_donate".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $starts
 * @property string $ends
 *
 * @property TblUsersProfile $idUser
 */
class UsersDonate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_donate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'starts'], 'required'],
            [['id_user'], 'integer'],
            [['starts', 'ends'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'starts' => 'Starts',
            'ends' => 'Ends',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(TblUsersProfile::className(), ['id' => 'id_user']);
    }
}
