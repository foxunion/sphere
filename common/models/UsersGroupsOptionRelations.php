<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_groups_option_relations".
 *
 * @property integer $id_packet
 * @property integer $id_packet_option
 *
 * @property TblUsersGroups $idPacket
 * @property TblUsersGroupsOptions $idPacketOption
 */
class UsersGroupsOptionRelations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_groups_option_relations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_packet', 'id_packet_option'], 'required'],
            [['id_packet', 'id_packet_option'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_packet' => 'Id Packet',
            'id_packet_option' => 'Id Packet Option',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacket()
    {
        return $this->hasOne(TblUsersGroups::className(), ['id' => 'id_packet']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacketOption()
    {
        return $this->hasOne(TblUsersGroupsOptions::className(), ['id' => 'id_packet_option']);
    }
}
