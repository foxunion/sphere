<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_services_data".
 *
 * @property integer $id
 * @property integer $id_service
 * @property integer $id_user
 * @property integer $id_unit
 * @property string $avg_price
 * @property string $price
 * @property string $image
 * @property string $worktime
 * @property string $description
 * @property integer $discount
 * @property integer $is_qualitymark
 * @property integer $is_recomendated
 *
 * @property TblUsers $idUser
 * @property TblServices $idService
 * @property TblServicesUnits $idUnit
 * @property TblUsersMyViews[] $tblUsersMyViews
 */
class ServicesData extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public $_image;
    public $_deleteimage;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_services_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_service', 'id_user', 'id_unit'], 'required'],
            [['id_service', 'id_user', 'id_unit', 'discount', 'is_qualitymark', 'is_recomendated'], 'integer'],
            [['description'], 'safe'],
            [['worktime'], 'safe'],
            [['avg_price', 'price'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_service' => 'Сфера',
            'id_user' => 'Пользователь',
            'id_unit' => 'Величина измерения',
            'avg_price' => 'Цена от',
            'price' => 'Цена',
            '_image' => 'Изображение',
            'worktime' => 'Время работы',
            'description' => 'Описание',
            'discount' => 'Скидка',
            'is_qualitymark' => 'Знак качества',
            'is_recomendated' => 'Рекомендуем',
            '_deleteimage' => 'Удалить изображение'
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'id_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(ServicesUnits::className(), ['id' => 'id_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersMyViews()
    {
        return $this->hasMany(UsersMyViews::className(), ['id_item' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removeImages();
            $this->clearImagesCache();
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $values)
    {

        $this->_image = UploadedFile::getInstance($this, '_image');
        if($this->_image !== null && $this->_image->tempName !== '' && $this->_image instanceof UploadedFile) { // проверяем есть ли загруженное изображение
            if (!$this->isNewRecord){
                $this->removeImages(); // удаляем старое изображение 
            }
            $this->attachImage($this->_image->tempName);
            $this->clearImagesCache();
            $this->_image->reset();
        }
        if( $this->_deleteimage )
        {
            $this->removeImages();
            $this->clearImagesCache();
        }

        if (parent::afterSave($insert, $values)) {
            return true;
        } else {
            return false;
        }
    }
}
