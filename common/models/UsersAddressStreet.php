<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_address_street".
 *
 * @property integer $id
 * @property integer $id_area
 * @property string $title
 * @property integer $is_visible
 *
 * @property TblUsersAddress[] $tblUsersAddresses
 * @property TblUsersAddressArea $idArea
 */
class UsersAddressStreet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_address_street';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_area', 'title'], 'required'],
            [['id_area', 'is_visible'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_area' => 'Исторический район',
            'title' => 'Заголовок',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersAddresses()
    {
        return $this->hasMany(TblUsersAddress::className(), ['id_street' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdArea()
    {
        return $this->hasOne(TblUsersAddressArea::className(), ['id' => 'id_area']);
    }
}
