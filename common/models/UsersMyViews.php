<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_my_views".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_item
 * @property integer $is_bookmark
 * @property string $created
 * @property string $updated
 *
 * @property TblUsers $idUser
 * @property TblServicesData $idItem
 */
class UsersMyViews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_my_views';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_item', 'created'], 'required'],
            [['id_user', 'id_item', 'is_bookmark'], 'integer'],
            [['created', 'updated'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_item' => 'Id Item',
            'is_bookmark' => 'Is Bookmark',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(TblUsers::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdItem()
    {
        return $this->hasOne(TblServicesData::className(), ['id' => 'id_item']);
    }
}
