<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_reviews".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $text
 * @property integer $is_visible
 * @property string $created
 *
 * @property TbLReviewsImagesHasTblReviews $tbLReviewsImagesHasTblReviews
 * @property TblReviewsImages[] $tbLReviewsImages
 * @property TblUsers $idUser
 * @property TblReviewsImageRelations[] $tblReviewsImageRelations
 * @property TblReviewsRatingRelations[] $tblReviewsRatingRelations
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'text'], 'required'],
            [['id_user', 'id_service', 'id_rating', 'is_visible'], 'integer'],
            [['text'], 'string'],
            [['created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Пользователь',
            'id_service' => 'Услуга',
            'id_rating' => 'Шаблон ответа',
            'text' => 'Сообщение',
            'is_visible' => 'Видимость',
            'created' => 'Добавлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbLReviewsImagesHasTblReviews()
    {
        return $this->hasOne(TbLReviewsImagesHasTblReviews::className(), ['tbl_reviews_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbLReviewsImages()
    {
        return $this->hasMany(TblReviewsImages::className(), ['id' => 'tbL_reviews_images_id'])->viaTable('tbL_reviews_images_has_tbl_reviews', ['tbl_reviews_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

    public function getIdService()
    {
        return $this->hasOne(Services::className(), ['id' => 'id_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblReviewsImageRelations()
    {
        return $this->hasMany(TblReviewsImageRelations::className(), ['id_review' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblReviewsRatingRelations()
    {
        return $this->hasMany(TblReviewsRatingRelations::className(), ['id_review' => 'id']);
    }
}
