<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_views".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $created
 *
 * @property TblUsers $idUser
 */
class UsersViews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_views';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'created'], 'required'],
            [['id_user'], 'integer'],
            [['created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(TblUsers::className(), ['id' => 'id_user']);
    }
}
