<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_address_city".
 *
 * @property integer $id
 * @property integer $id_region
 * @property string $title
 * @property integer $is_visible
 *
 * @property TblUsersAddress[] $tblUsersAddresses
 * @property TblUsersAddressArea[] $tblUsersAddressAreas
 * @property TblUsersAddressRegion $idRegion
 */
class UsersAddressCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_address_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_region', 'title'], 'required'],
            [['id_region', 'is_visible'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_region' => 'Регион',
            'title' => 'Заголовок',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersAddresses()
    {
        return $this->hasMany(TblUsersAddress::className(), ['id_city' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersAddressAreas()
    {
        return $this->hasMany(TblUsersAddressArea::className(), ['id_city' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRegion()
    {
        return $this->hasOne(TblUsersAddressRegion::className(), ['id' => 'id_region']);
    }
}
