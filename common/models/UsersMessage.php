<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_message".
 *
 * @property integer $id
 * @property integer $id_sender
 * @property integer $id_recipient
 * @property string $subject
 * @property string $text
 * @property integer $type
 * @property integer $status
 * @property string $created
 * @property string $updated
 *
 * @property TblUsers $idSender
 * @property TblUsers $idRecipient
 */
class UsersMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sender', 'id_recipient'], 'required'],
            [['id_sender', 'id_recipient', 'type', 'status'], 'integer'],
            [['text'], 'string'],
            [['created', 'updated'], 'safe'],
            [['subject'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sender' => 'Отправитель',
            'id_recipient' => 'Получатель',
            'subject' => 'Тема',
            'text' => 'Сообщение',
            'type' => 'Тип',
            'status' => 'Статус',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSender()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_sender']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecipient()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_recipient']);
    }
}
