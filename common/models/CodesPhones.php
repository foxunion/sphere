<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_codes_phones".
 *
 * @property integer $id
 * @property string $title
 * @property string $value
 * @property integer $is_visible
 */
class CodesPhones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_codes_phones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_visible'], 'integer'],
            [['title', 'value'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'value' => 'Код',
            'is_visible' => 'Видимость',
        ];
    }
}
