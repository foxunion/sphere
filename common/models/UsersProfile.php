<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
/**
 * This is the model class for table "tbl_users_profile".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_status
 * @property string $title
 * @property string $image
 * @property double $balance
 * @property integer $bonus
 * @property string $url_vk
 * @property string $url_www
 * @property string $schedule
 *
 * @property TblUsersDonate[] $tblUsersDonates
 * @property TblUsersPhones[] $tblUsersPhones
 * @property TblUsers $idUser
 * @property TblUsersRating[] $tblUsersRatings
 */
class UsersProfile extends \yii\db\ActiveRecord
{
    public $_image;
    public $_deleteimage;

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user', 'id_status', 'bonus'], 'integer'],
            [['balance'], 'number'],
            [['schedule'], 'safe'],
            [['title'], 'string', 'max' => 128],
            [['image'], 'string', 'max' => 255],
            [['url_vk', 'url_www'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Пользователь',
            'id_status' => 'Статус',
            'title' => 'Заголовок',
            '_image' => 'Изображение',
            'balance' => 'Баланс',
            'bonus' => 'Бонус',
            'url_vk' => 'Ссылка вконтакте',
            'url_www' => 'Ссылка личный сайт',
            'schedule' => 'Время работы',
            '_deleteimage' => 'Удалить изображение'
        ];
    }

    public function getTypeOrganization()
    {
        return $this->hasOne(TypeOrganization::className(), ['id' => 'id_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersDonates()
    {
        return $this->hasMany(UsersDonate::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

     public function getStatus()
    {
        return $this->hasOne(UsersStatus::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersRatings()
    {
        return $this->hasMany(UsersRating::className(), ['id_user' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removeImages();
            $this->clearImagesCache();
            return true;
        } else {
            return false;
        }
    }

     public function afterSave($insert, $values)
    {
         $this->_image = UploadedFile::getInstance($this, '_image');
        if($this->_image !== null && $this->_image->tempName !== '' && $this->_image instanceof UploadedFile) { // проверяем есть ли загруженное изображение
            if (!$this->isNewRecord){
                $this->removeImages(); // удаляем старое изображение 
            }
            $this->attachImage($this->_image->tempName);
            $this->clearImagesCache();
            $this->_image->reset();
        }
        if( $this->_deleteimage )
        {
            $this->removeImages();
            $this->clearImagesCache();
        }

        if (parent::afterSave($insert, $values)) {
            return true;
        } else {
            return false;
        }
    }

}
