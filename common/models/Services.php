<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_services".
 *
 * @property integer $id
 * @property integer $id_category
 * @property string $title
 * @property text $description
 * @property string $keyword
 * @property string $image
 * @property integer $is_visible
 *
 * @property TblServicesCategory $idCategory
 * @property TblServicesData[] $tblServicesDatas
 * @property TblServicesSimilar[] $tblServicesSimilars
 */
class Services extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public $_image;
    public $_deleteimage;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_category', 'title'], 'required'],
            [['id_category', 'is_visible'], 'integer'],
            [['title'], 'string', 'max' => 128],
            [['image'], 'string', 'max' => 255],
            [['description','keyword'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_category' => 'Категория',
            'title' => 'Заголовок',
            'description' => "Описание",
            'keyword' => 'Теги для поиска',
            '_image' => 'Изображение',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ServicesCategory::className(), ['id' => 'id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesDatas()
    {
        return $this->hasMany(ServicesData::className(), ['id_service' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesSimilars()
    {
        return $this->hasMany(ServicesSimilar::className(), ['id_service' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removeImages();
            $this->clearImagesCache();
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $values)
    {

        $this->_image = UploadedFile::getInstance($this, '_image');
        if($this->_image !== null && $this->_image->tempName !== '' && $this->_image instanceof UploadedFile) { // проверяем есть ли загруженное изображение
            if (!$this->isNewRecord){
                $this->removeImages(); // удаляем старое изображение 
            }
            $this->attachImage($this->_image->tempName);
            $this->clearImagesCache();
            $this->_image->reset();
        }
        if( $this->_deleteimage )
        {
            $this->removeImages();
            $this->clearImagesCache();
        }

        if (parent::afterSave($insert, $values)) {
            return true;
        } else {
            return false;
        }
    }
}
