<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_groups".
 *
 * @property integer $id
 * @property string $title
 * @property integer $price
 * @property integer $is_visible
 *
 * @property TblUsersGroupsOptionRelations[] $tblUsersGroupsOptionRelations
 * @property TblUsersGroupsRelations[] $tblUsersGroupsRelations
 * @property TblUsersGroupsRules[] $tblUsersGroupsRules
 */
class UsersGroups extends \yii\db\ActiveRecord
{

    public $options;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['price', 'is_visible'], 'integer'],
            [['title'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'price' => 'Цена',
            'is_visible' => 'Видимость',
            'options' => 'Опции',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersGroupsOptionRelations()
    {
        return $this->hasMany(TblUsersGroupsOptionRelations::className(), ['id_packet' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersGroupsRelations()
    {
        return $this->hasMany(TblUsersGroupsRelations::className(), ['id_group' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersGroupsRules()
    {
        return $this->hasMany(TblUsersGroupsRules::className(), ['id_group' => 'id']);
    }
}
