<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_services_images".
 *
 * @property integer $id
 * @property integer $id_service
 * @property integer $id_user
 * @property string $image
 * @property string $description
 *
 * @property TblServicesCategory $idService
 * @property TblUsers $idUser
 */
class ServicesImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_services_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_service', 'id_user', 'image'], 'required'],
            [['id_service', 'id_user'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_service' => 'Id Service',
            'id_user' => 'Id User',
            'image' => 'Изображение',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdService()
    {
        return $this->hasOne(TblServicesCategory::className(), ['id' => 'id_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(TblUsers::className(), ['id' => 'id_user']);
    }
}
