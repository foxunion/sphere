<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_address_area".
 *
 * @property integer $id
 * @property integer $id_city
 * @property string $title
 * @property integer $is_visible
 *
 * @property TblUsersAddress[] $tblUsersAddresses
 * @property TblUsersAddressCity $idCity
 * @property TblUsersAddressStreet[] $tblUsersAddressStreets
 */
class UsersAddressArea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_address_area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_city'], 'required'],
            [['id_city', 'is_visible'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_city' => 'Город',
            'title' => 'Заголовок',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersAddresses()
    {
        return $this->hasMany(TblUsersAddress::className(), ['id_area' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCity()
    {
        return $this->hasOne(TblUsersAddressCity::className(), ['id' => 'id_city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersAddressStreets()
    {
        return $this->hasMany(TblUsersAddressStreet::className(), ['id_area' => 'id']);
    }
}
