<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_users".
 *
 * @property integer $id
 * @property string $uid
 * @property string $email
 * @property string $password
 * @property string $created_at
 * @property string $updated_at
 *
 * @property TblReviews[] $tblReviews
 * @property TblServicesData[] $tblServicesDatas
 * @property TblServicesImages[] $tblServicesImages
 * @property TblUsersAddress[] $tblUsersAddresses
 * @property TblUsersGroupsRelations[] $tblUsersGroupsRelations
 * @property TblUsersMessage[] $tblUsersMessages
 * @property TblUsersMyViews[] $tblUsersMyViews
 * @property TblUsersProfile[] $tblUsersProfiles
 * @property TblUsersViews[] $tblUsersViews
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const ROLE_USER = 10;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public $new_password;
    public $new_confirm;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'new_password'], 'required'],
            [['uid'], 'string', 'max' => 45],
            [['email', 'new_password'], 'string', 'max' => 255],
            [['email', 'uid'], 'unique'],
            [['new_password'], 'string', 'min'=>6, 'max'=>128],
            // array('new_confirm', 'compare', 'compareAttribute'=>'new_password'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Уникальное имя пользователя',
            'email' => 'Электронная почта',
            'new_password' => 'Пароль',
            'created_at' => 'Created',
            'updated_at' => 'Updated',
        ];
    }


    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->salt;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesDatas()
    {
        return $this->hasMany(ServicesData::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesImages()
    {
        return $this->hasMany(ServicesImages::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersAddresses()
    {
        return $this->hasMany(UsersAddress::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersGroupsRelations()
    {
        return $this->hasMany(UsersGroupsRelations::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersMessages()
    {
        return $this->hasMany(UsersMessage::className(), ['id_recipient' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersMyViews()
    {
        return $this->hasMany(UsersMyViews::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersProfile()
    {
        return $this->hasOne(UsersProfile::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersViews()
    {
        return $this->hasMany(UsersViews::className(), ['id' => 'id_user']);
    }

    public function getUsersPhones()
    {
        return $this->hasMany(UsersPhones::className(), ['id_user' => 'id']);
    }

    public function beforeSave($insert) 
    { 
        if ($this->new_password) { 
            $this->salt = $this->generateSalt();
            $this->password = $this->hashPassword($this->new_password, $this->salt);
        }
        if (parent::beforeSave($insert)) {
            return true;
        } else {
            return false;
        }
    }
    
    
    // Проверка валидности пароля
    public function validatePassword($password)
    {
        return $this->hashPassword($password,$this->salt)===$this->password;
    }

    // Создание хэша пароля
    public function hashPassword($password,$salt)
    {
        return md5($salt.$password);
    }

    // Генерация "соли". Этот метод генерирует случайным образом слово
    // заданной длины. Длина указывается в единственном свойстве метода.
    // Символы, применяемые при генерации, указаны в переменной $chars.
    // По умолчанию, длина соли 32 символа.
    public function generateSalt($length=32)
    {
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime()*1000000);
        $i = 1;
        $salt = '';

        while ($i <= $length)
        {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $salt .= $tmp;
            $i++;
        }
        return $salt;
    } 
}
