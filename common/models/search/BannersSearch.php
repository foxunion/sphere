<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Banners;

/**
 * BannersSearch represents the model behind the search form about `common\models\Banners`.
 */
class BannersSearch extends Banners
{
    public function rules()
    {
        return [
            [['id', 'is_visible'], 'integer'],
            [['title', 'reference', 'image', 'place'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Banners::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_visible' => $this->is_visible,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'reference', $this->reference])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'place', $this->place]);

        return $dataProvider;
    }
}
