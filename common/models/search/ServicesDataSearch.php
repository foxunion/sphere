<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ServicesData;

/**
 * ServicesDataSearch represents the model behind the search form about `common\models\ServicesData`.
 */
class ServicesDataSearch extends ServicesData
{
    public function rules()
    {
        return [
            [['id', 'id_service', 'id_user', 'id_unit', 'discount', 'is_qualitymark', 'is_recomendated'], 'integer'],
            [['avg_price', 'price', 'image', 'worktime', 'description'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ServicesData::find()->where("id_user = " . $params['id']);;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_service' => $this->id_service,
            'id_user' => $this->id_user,
            'id_unit' => $this->id_unit,
            'worktime' => $this->worktime,
            'discount' => $this->discount,
            'is_qualitymark' => $this->is_qualitymark,
            'is_recomendated' => $this->is_recomendated,
        ]);

        $query->andFilterWhere(['like', 'avg_price', $this->avg_price])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
