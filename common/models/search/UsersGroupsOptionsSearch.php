<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UsersGroupsOptions;

/**
 * UsersGroupsOptionsSearch represents the model behind the search form about `common\models\UsersGroupsOptions`.
 */
class UsersGroupsOptionsSearch extends UsersGroupsOptions
{
    public function rules()
    {
        return [
            [['id', 'is_visible'], 'integer'],
            [['title', 'desc'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UsersGroupsOptions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_visible' => $this->is_visible,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'desc', $this->desc]);

        return $dataProvider;
    }
}
