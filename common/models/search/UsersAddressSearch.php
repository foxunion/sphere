<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UsersAddress;

/**
 * UsersAddressSearch represents the model behind the search form about `common\models\UsersAddress`.
 */
class UsersAddressSearch extends UsersAddress
{
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_region', 'id_city', 'id_street', 'id_area', 'apartment', 'is_map'], 'integer'],
            [['house'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UsersAddress::find()->where("id_user = " . $params['id']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'id_region' => $this->id_region,
            'id_city' => $this->id_city,
            'id_street' => $this->id_street,
            'id_area' => $this->id_area,
            'apartment' => $this->apartment,
            'is_map' => $this->is_map,
        ]);

        $query->andFilterWhere(['like', 'house', $this->house]);

        return $dataProvider;
    }
}
