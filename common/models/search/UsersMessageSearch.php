<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UsersMessage;

/**
 * UsersMessageSearch represents the model behind the search form about `common\models\UsersMessage`.
 */
class UsersMessageSearch extends UsersMessage
{
    public function rules()
    {
        return [
            [['id', 'id_sender', 'id_recipient', 'type', 'status'], 'integer'],
            [['subject', 'text', 'created', 'updated'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UsersMessage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_sender' => $this->id_sender,
            'id_recipient' => $this->id_recipient,
            'type' => $this->type,
            'status' => $this->status,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
