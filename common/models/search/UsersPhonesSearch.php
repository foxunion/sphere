<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UsersPhones;

/**
 * UsersPhonesSearch represents the model behind the search form about `common\models\UsersPhones`.
 */
class UsersPhonesSearch extends UsersPhones
{
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_code'], 'integer'],
            [['phone'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UsersPhones::find()->where("id_user = " . $params['id']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'id_code' => $this->id_code,
        ]);

        $query->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
