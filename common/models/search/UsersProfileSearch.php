<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UsersProfile;

/**
 * UsersProfileSearch represents the model behind the search form about `common\models\UsersProfile`.
 */
class UsersProfileSearch extends UsersProfile
{
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_status', 'bonus'], 'integer'],
            [['title', 'image', 'url_vk', 'url_www', 'schedule'], 'safe'],
            [['balance'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UsersProfile::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'id_status' => $this->id_status,
            'balance' => $this->balance,
            'bonus' => $this->bonus,
            'schedule' => $this->schedule,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'url_vk', $this->url_vk])
            ->andFilterWhere(['like', 'url_www', $this->url_www]);

        return $dataProvider;
    }
}
