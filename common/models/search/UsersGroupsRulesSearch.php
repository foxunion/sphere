<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UsersGroupsRules;

/**
 * UsersGroupsRulesSearch represents the model behind the search form about `common\models\UsersGroupsRules`.
 */
class UsersGroupsRulesSearch extends UsersGroupsRules
{
    public function rules()
    {
        return [
            [['id', 'id_group'], 'integer'],
            [['table', 'option'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UsersGroupsRules::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_group' => $this->id_group,
        ]);

        $query->andFilterWhere(['like', 'table', $this->table])
            ->andFilterWhere(['like', 'option', $this->option]);

        return $dataProvider;
    }
}
