<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UsersDonate as UsersDonateModel;

/**
 * UsersDonate represents the model behind the search form about `common\models\UsersDonate`.
 */
class UsersDonate extends UsersDonateModel
{
    public function rules()
    {
        return [
            [['id', 'id_user'], 'integer'],
            [['starts', 'ends'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = UsersDonateModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'starts' => $this->starts,
            'ends' => $this->ends,
        ]);

        return $dataProvider;
    }
}
