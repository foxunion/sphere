<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_groups_options".
 *
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property integer $is_visible
 *
 * @property TblUsersGroupsOptionRelations[] $tblUsersGroupsOptionRelations
 */
class UsersGroupsOptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_groups_options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['desc'], 'string'],
            [['is_visible'], 'integer'],
            [['title'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'desc' => 'Описание',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersGroupsOptionRelations()
    {
        return $this->hasMany(TblUsersGroupsOptionRelations::className(), ['id_packet_option' => 'id']);
    }
}
