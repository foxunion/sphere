<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_site_config}}".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property string $content
 */
class SiteConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_site_config}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'title', 'content'], 'required'],
            [['content'], 'string'],
            [['alias', 'title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'title' => 'Title',
            'content' => 'Content',
        ];
    }
}
