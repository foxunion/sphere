<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_services_files".
 *
 * @property integer $id
 * @property integer $id_service
 * @property integer $id_user
 * @property string $file
 */
class ServicesFiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_services_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_service', 'id_user', 'file'], 'required'],
            [['id_service', 'id_user'], 'integer'],
            [['file'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_service' => 'Id Service',
            'id_user' => 'Id User',
            'file' => 'Файл',
        ];
    }
}
