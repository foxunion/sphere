<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_rating".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $vote
 *
 * @property TblUsersProfile $idUser
 */
class UsersRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'vote'], 'required'],
            [['id_user', 'vote'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'vote' => 'Vote',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(TblUsersProfile::className(), ['id' => 'id_user']);
    }
}
