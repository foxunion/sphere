<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_type_organization".
 *
 * @property integer $id
 * @property string $title
 * @property string $value
 * @property integer $is_visible
 */
class TypeOrganization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_type_organization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_visible'], 'integer'],
            [['title', 'value'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Полное название типа организации',
            'value' => 'Аббревиатура типа организации',
            'is_visible' => 'Видимость',
        ];
    }
}
