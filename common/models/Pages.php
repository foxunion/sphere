<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_pages".
 *
 * @property integer $id
 * @property integer $id_parent
 * @property string $path
 * @property string $title
 * @property string $text
 * @property integer $is_visible
 *
 * @property Pages $idParent
 * @property Pages[] $pages
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent', 'is_visible'], 'integer'],
            [['title'], 'required'],
            [['text'], 'string'],
            [['path', 'title'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent' => 'Родитель',
            'path' => 'Путь',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdParent()
    {
        return $this->hasOne(Pages::className(), ['id' => 'id_parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Pages::className(), ['id_parent' => 'id']);
    }
}
