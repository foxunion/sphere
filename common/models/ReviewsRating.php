<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_reviews_rating".
 *
 * @property integer $id
 * @property string $title
 * @property integer $rate
 * @property integer $is_visible
 *
 * @property TblReviewsRatingRelations[] $tblReviewsRatingRelations
 */
class ReviewsRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_reviews_rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'rate'], 'required'],
            [['rate', 'is_visible'], 'integer'],
            [['title'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'rate' => 'Голос ',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblReviewsRatingRelations()
    {
        return $this->hasMany(TblReviewsRatingRelations::className(), ['id_rating' => 'id']);
    }
}
