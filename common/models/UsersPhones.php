<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_phones".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_code
 * @property string $phone
 * @property string $title
 *
 * @property TblUsersProfile $idUser
 */
class UsersPhones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_phones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user', 'id_code'], 'integer'],
            [['phone'], 'string', 'max' => 45],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_code' => 'Код',
            'phone' => 'Тел',
            'title' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UsersProfile::className(), ['id' => 'id_user']);
    }

    public function getCode()
    {
        return $this->hasOne(CodesPhones::className(), ['id' => 'id_code']);
    }

    public function getText()
    {
        $text = '';
        if($this->title)
            $text .= $this->title.": ";
        else
            $text .= "Телефон: ";
        $text .= $this->code->title;
        $text .= " ".$this->phone." ";
        return $text;
    }
}
