<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_address_region".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_visible
 *
 * @property TblUsersAddress[] $tblUsersAddresses
 * @property TblUsersAddressCity[] $tblUsersAddressCities
 */
class UsersAddressRegion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_address_region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['id', 'is_visible'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersAddresses()
    {
        return $this->hasMany(TblUsersAddress::className(), ['id_region' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersAddressCities()
    {
        return $this->hasMany(TblUsersAddressCity::className(), ['id_region' => 'id']);
    }
}
