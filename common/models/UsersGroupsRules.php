<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_groups_rules".
 *
 * @property integer $id
 * @property integer $id_group
 * @property string $table
 * @property string $option
 *
 * @property TblUsersGroups $idGroup
 */
class UsersGroupsRules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_groups_rules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_group', 'table', 'option'], 'required'],
            [['id', 'id_group'], 'integer'],
            [['table', 'option'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_group' => 'Группа',
            'table' => 'Таблица',
            'option' => 'Опции',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGroup()
    {
        return $this->hasOne(TblUsersGroups::className(), ['id' => 'id_group']);
    }
}
