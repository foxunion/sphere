<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_banners".
 *
 * @property integer $id
 * @property string $title
 * @property string $reference
 * @property string $image
 * @property string $place
 * @property integer $is_visible
 */
class Banners extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public $_image;
    public $_deleteimage;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_banners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['image'], 'file', 'types' => 'jpg, png'],
            [['is_visible', '_deleteimage'], 'integer'],
            [['title'], 'string', 'max' => 128],
            [['reference', 'image'], 'string', 'max' => 255],
            [['place'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'reference' => 'Ссылка',
            '_image' => 'Изображение',
            'place' => 'Место',
            'is_visible' => 'Видимость',
            '_deleteimage' => 'Удалить изображение',
        ];
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removeImages();
            $this->clearImagesCache();
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $values)
    {
         $this->_image = UploadedFile::getInstance($this, '_image');
        if($this->_image !== null && $this->_image->tempName !== '' && $this->_image instanceof UploadedFile) { // проверяем есть ли загруженное изображение
            if (!$this->isNewRecord){
                $this->removeImages(); // удаляем старое изображение 
            }
            $this->attachImage($this->_image->tempName);
            $this->clearImagesCache();
            $this->_image->reset();
        }
        if( $this->_deleteimage )
        {
            $this->removeImages();
            $this->clearImagesCache();
        }

        if (parent::afterSave($insert)) {
            return true;
        } else {
            return false;
        }
    }

}
