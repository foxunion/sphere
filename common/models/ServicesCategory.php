<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;


/**
 * This is the model class for table "tbl_services_category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $keyword
 * @property string $img
 * @property integer $position
 * @property integer $is_visible
 *
 * @property TblServiceFiles[] $tblServiceFiles
 * @property TblServices[] $tblServices
 * @property ServicesCategory $parent
 * @property ServicesCategory[] $servicesCategories
 * @property TblServicesImages[] $tblServicesImages
 */
class ServicesCategory extends \yii\db\ActiveRecord
{
	public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public $_image;
    public $_deleteimage;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_services_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'position', 'is_visible'], 'integer'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 128],
            [['keyword'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родитель',
            'title' => 'Заголовок',
            'keyword' => 'Теги для поиска',
            '_image' => 'Изображение',
            'position' => 'Позиция',
            'is_visible' => 'Видимость',
            '_deleteimage' => 'Удалить изображение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblServiceFiles()
    {
        return $this->hasMany(TblServiceFiles::className(), ['id_service' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Services::className(), ['id_category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ServicesCategory::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesCategories()
    {
        return $this->hasMany(ServicesCategory::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblServicesImages()
    {
        return $this->hasMany(TblServicesImages::className(), ['id_service' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removeImages();
            $this->clearImagesCache();
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $values)
    {

        $this->_image = UploadedFile::getInstance($this, '_image');
        if($this->_image !== null && $this->_image->tempName !== '' && $this->_image instanceof UploadedFile) { // проверяем есть ли загруженное изображение
            if (!$this->isNewRecord){
                $this->removeImages(); // удаляем старое изображение 
            }
            $this->attachImage($this->_image->tempName);
            $this->clearImagesCache();
            $this->_image->reset();
        }
        if( $this->_deleteimage )
        {
            $this->removeImages();
            $this->clearImagesCache();
        }

        if (parent::afterSave($insert, $values)) {
            return true;
        } else {
            return false;
        }
    }
}
