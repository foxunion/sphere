<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_users_address".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $description
 * @property integer $id_region
 * @property integer $id_city
 * @property integer $id_street
 * @property integer $id_area
 * @property string $house
 * @property integer $apartment
 * @property integer $is_map
 * description
 *
 * @property TblUsers $idUser
 * @property TblUsersAddressRegion $idRegion
 * @property TblUsersAddressCity $idCity
 * @property TblUsersAddressStreet $idStreet
 * @property TblUsersAddressArea $idArea
 */
class UsersAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_region', 'id_city', 'id_street', 'id_area', 'apartment', 'is_map'], 'integer'],
            [['house'], 'string', 'max' => 10],
            [['description'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Пользователь',
            'description' => "Описание",
            'id_region' => 'Регион',
            'id_city' => 'Город',
            'id_street' => 'Улица',
            'id_area' => 'Район',
            'house' => 'Дом',
            'apartment' => 'Офис',
            'is_map' => 'Показывать на карте',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(UsersAddressRegion::className(), ['id' => 'id_region']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(UsersAddressCity::className(), ['id' => 'id_city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreet()
    {
        return $this->hasOne(UsersAddressStreet::className(), ['id' => 'id_street']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(UsersAddressArea::className(), ['id' => 'id_area']);
    }

    public function getText()
    {
        $text = '';
        if($this->description)
            $text .= $this->description.": ";
        else
            $text .= "Адрес: ";
        $text .= $this->region->title.', г.'.$this->city->title.", ул.".$this->street->title.", ";
        $text .= "д. ".$this->house.", ";
        if( $this->apartment!="" )
            $text .= "оф. ".$this->apartment." ";
        $text .=" (".$this->area->title.") ";
        return $text;
    }

    public function getGeoText()
    {
        $text = '';
        $text .= $this->region->title.', '.$this->city->title.", ".$this->street->title." ".$this->house;
        return $text;
    }
}
