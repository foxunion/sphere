<?php
namespace common\helpers;

class YiiTextHelper
{
    /**
   * Устанавливает лимит символов
   * @param string $str
   * @param integer $n
   * @param string $end_char
   * @return string 
   */
  static function character_limiter($str, $n = 500, $end_char = '&#8230;') {
      if (strlen($str) < $n) {
          return $str;
      }

      $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

      if (strlen($str) <= $n) {
          return $str;
      }

      $out = "";
      foreach (explode(' ', trim($str)) as $val) {
          $out .= $val . ' ';

          if (strlen($out) >= $n) {
              $out = trim($out);
              return (strlen($out) == strlen($str)) ? $out : $out . $end_char;
          }
      }
  }

  /**
   * Экринирует строку
   *
   * @param  string  $string    экранируемая строка
   * @return string
   */
    static function escape($string) {
        $string = strval($string);
          if ($string) {
              $string = htmlspecialchars($string);
          }
          return $string;
    }

  /**
   * Перевод русского текста в транслит
   * @param string $str - форматируемый текст
   * @param integer $character_limiter - лимит символов
   * @return string
   */
  static function translite($str, $character_limiter = "60") {

      $str = self::escape($str);
      $str = trim($str);
      $str = strtolower($str);
      $str = self::character_limiter($str, $character_limiter, "");

      $translit = array("а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ж" => "zh", "з" => "z",
          "и" => "i", "й" => "y", "к" => "k", "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p",
          "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h", "ц" => "c", "ч" => "ch",
          "ш" => "sh", "щ" => "sch", "ъ" => "", "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
          "А" => "a", "Б" => "b", "В" => "v", "Г" => "g", "Д" => "d", "Е" => "e", "Ж" => "zh", "З" => "z",
          "И" => "i", "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n", "О" => "o", "П" => "p",
          "Р" => "r", "С" => "s", "Т" => "t", "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "c", "Ч" => "ch",
          "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "y", "Ь" => "", "Э" => "e", "Ю" => "yu", "Я" => "ya",
          " " => "-", "," => "");
      $str = strtr($str, $translit);
      return $str;
  }

  static function plural($count, $input) {
    $count = (int) $count;
    $l2 = substr($count, -2);
    $l1 = substr($count, -1);
    if ($l2 > 10 && $l2 < 20)
        return $input[0];
    else
        switch ($l1) {
            case 0: return $input[0];
                break;
            case 1: return $input[1];
                break;
            case 2: case 3: case 4: return $input[2];
                break;
            default: return $input[0];
                break;
        }
    return false;
    }
}
?>